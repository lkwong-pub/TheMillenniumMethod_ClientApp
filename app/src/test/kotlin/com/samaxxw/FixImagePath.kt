package com.samaxxw

import com.samaxxw.app.DemoResourceServerApplication
import com.samaxxw.controller.searchModel.PageResourcesSearchModel
import com.samaxxw.dtoservice.PageResourcesDtoService
import com.samaxxw.mongo.dao.PageResourcesDao
import com.samaxxw.mongo.model.PageResources
import com.samaxxw.util.PaginationUtils
import org.junit.Assert
import org.junit.Ignore
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner
import org.springframework.transaction.annotation.Transactional

@SpringBootTest(classes = [DemoResourceServerApplication::class])
@ActiveProfiles("dev")
@RunWith(SpringJUnit4ClassRunner::class)
class FixImagePath {

    @Autowired
    val pageResourcesDao : PageResourcesDao? = null

    @Autowired
    val pageResourcesDtoService : PageResourcesDtoService? = null

    @Test
    @Transactional
    fun fixImagePath() {

        val result = pageResourcesDao?.findAll()

        if (result != null) {
            result.forEach {
                if (it.content!!.contains("../images/right_foot.jpg")) {
                    println("[section - ${it.section}] [name - ${it.name}]")
                    it.content = it.content!!.replace("../images/right_foot.jpg", "/images/right_foot.jpg")
                    pageResourcesDao?.save(it)
                    println("${it.content}")
                    println("=============================================")
                }
            }
        }
        Assert.assertTrue(result!!.size > 0)

    }

}