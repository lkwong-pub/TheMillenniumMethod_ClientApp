package com.samaxxw

import com.samaxxw.app.DemoResourceServerApplication
import com.samaxxw.model.Role
import com.samaxxw.model.RolePermissionMap
import com.samaxxw.repository.RolePermissionMapRepository
import com.samaxxw.repository.RoleRepository
import com.samaxxw.service.MailService
import org.junit.Assert
import org.junit.Ignore
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.mail.SimpleMailMessage
import org.springframework.mail.javamail.JavaMailSender
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner
import org.springframework.mail.javamail.MimeMessageHelper




@SpringBootTest(classes = [DemoResourceServerApplication::class])
@ActiveProfiles("dev")
@RunWith(SpringJUnit4ClassRunner::class)
class EmailServiceTest {

    @Autowired
    val emailSender: JavaMailSender? = null

    @Autowired
    val mailService: MailService? = null

    @Test
    fun sendSimpleMessage() {

        var message = SimpleMailMessage()
        message.setTo("lkwong1013@gmail.com")
        message.setSubject("Test Message")
        message.setText("Test")
        emailSender?.send(message)
    }

    @Test
    fun sendMimeMessage() {

        var mimeMessage = emailSender?.createMimeMessage()!!

        val helper = MimeMessageHelper(mimeMessage, false, "utf-8")

        mimeMessage.setContent("<h1>Test</h1><br>Test message", "text/html")

//        var message = SimpleMailMessage()
        helper.setFrom("TheMillenniumMethod_Dev")
        helper.setTo("lkwong1013@gmail.com")
        helper.setSubject("Test Message")

        emailSender?.send(mimeMessage)
    }

    @Test
    fun sendMimeMessage_serviceTest_expected_success() {

        mailService?.sendEmailMessage("lkwong1013@gmail.com", "Test Message Service Test", "<h1>Test Service Test</h1><br>Test message service test")
        println("[sendMimeMessage_serviceTest_expected_success] end")
    }

}