package com.samaxxw

import com.samaxxw.app.DemoResourceServerApplication
import com.samaxxw.controller.searchModel.PageResourcesSearchModel
import com.samaxxw.dtoservice.PageResourcesDtoService
import com.samaxxw.mongo.dao.PageResourcesDao
import com.samaxxw.mongo.model.PageResources
import com.samaxxw.util.PaginationUtils
import org.junit.Assert
import org.junit.Ignore
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner
import org.springframework.transaction.annotation.Transactional

@SpringBootTest(classes = [DemoResourceServerApplication::class])
@ActiveProfiles("dev")
@RunWith(SpringJUnit4ClassRunner::class)
class MongoDataTest {

    @Autowired
    val pageResourcesDao : PageResourcesDao? = null

    @Autowired
    val pageResourcesDtoService : PageResourcesDtoService? = null

    @Test
    @Ignore
    @Transactional
    fun testInsertRecord_text() {

        val pageResources = createTextPageResource()
        pageResourcesDao?.save(pageResources)

        val pageResources2 = createTextPageResource_2()
        pageResourcesDao?.save(pageResources2)

        val pageResources3 = createTextPageResource_3()
        pageResourcesDao?.save(pageResources3)

        val result = pageResourcesDao?.findAll()

        Assert.assertTrue(result!!.size > 0)

    }

    @Test
    @Transactional
    fun testgetRecord() {

        val criteria = PageResourcesSearchModel()
        criteria.section = "s1"
        criteria.name = "sectionS1Title"
        val predicate = pageResourcesDtoService?.getPredicate(null, criteria)
        println("=== Using QueryDSL ===")
        val result = pageResourcesDao?.findAll(predicate!!)
        println("=== Without QueryDSL ===")
        val result2 = pageResourcesDao?.findAll()
        Assert.assertTrue(result!!.size > 0)

    }

    @Test
    @Transactional
    fun testgetRecord_2() {

        val criteria = PageResourcesSearchModel()
        criteria.section = "s1"
        criteria.name = "sectionS1Title"
        val predicate = pageResourcesDtoService?.getPredicate(null, criteria)
        val list = pageResourcesDtoService?.findAll(predicate!!, PaginationUtils.createPaginationDto(0, 100, "createDatetime", "desc"))
        println(list)
        Assert.assertTrue(list!!.content!!.size > 0)

    }

    @Test
    @Transactional
    fun testgetRecord_3() {

        val result = pageResourcesDao?.findAllBySectionAndLanguage("l1wtbnap", "en")
        println(result)
        Assert.assertTrue(result!!.size > 0)

    }

    @Test
    @Transactional
    fun testgetRecord_4() {

        val result = pageResourcesDao?.findAllByName("s1_content_1")
        println(result)
        Assert.assertTrue(result!!.size > 0)

    }


    fun createTextPageResource() : PageResources {
        val pageResources = PageResources()

        pageResources.section = "s1"
        pageResources.type = "text"
        pageResources.name = "sectionS1Title"
        pageResources.language = "en"
        pageResources.content = "Session 1 - Where the Bagpipe Notes are Positioned"

        return pageResources

    }

    fun createTextPageResource_2() : PageResources {
        val pageResources = PageResources()

        pageResources.section = "s2"
        pageResources.type = "text"
        pageResources.name = "sectionS2Title1"
        pageResources.language = "en"
        pageResources.content = "<strong>The Notes on the Lines</strong>"

        return pageResources

    }

    fun createTextPageResource_3() : PageResources {
        val pageResources = PageResources()

        pageResources.section = "s2"
        pageResources.type = "text"
        pageResources.name = "sectionS2Caption"
        pageResources.language = "en"
        pageResources.content = "<strong>B</strong>agpipe music is written upon a staff of 5 lines known as the short staff.  The highest note is <strong>High A</strong>. It is one line above the top of the staff and is distinguishable by having a line through the middle of the note; this small line is called a ledger line."

        return pageResources

    }

    fun createGroupPageResource() : PageResources {
        val pageResources = PageResources()

        pageResources.section = "s2"
        pageResources.type = "group"
        pageResources.name = "sectionS2" // camel case for parameter
        pageResources.language = "en"
        pageResources.content = "Section - S2"

        return pageResources

    }
}