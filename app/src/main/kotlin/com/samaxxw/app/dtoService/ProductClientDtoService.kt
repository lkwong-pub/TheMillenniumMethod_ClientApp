package com.samaxxw.app.dtoService

import com.querydsl.core.types.Predicate
import com.samaxxw.controller.searchModel.ProductSearchModel
import com.samaxxw.controller.searchModel.SearchModelName
import com.samaxxw.dtoservice.impl.ProductDtoServiceImpl
import com.samaxxw.model.QProduct
import com.samaxxw.util.ProductStatusEnum
import org.apache.commons.lang3.StringUtils
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import javax.servlet.http.HttpServletRequest

@Service
@Transactional
@Qualifier("productClientDtoService")
class ProductClientDtoService : ProductDtoServiceImpl() {

    override fun getPredicate(request: HttpServletRequest): Predicate {

        // Get Parameter directly from session
        var productSearchModel: ProductSearchModel?

        var name : String? = ""

        var category : String? = ""

        var status : Int? = ProductStatusEnum.PUBLISH.status

        if (request.session.getAttribute(SearchModelName.SEARCH_FILTER_NAME_PRODUCT) != null) {
            productSearchModel = request.session.getAttribute(SearchModelName.SEARCH_FILTER_NAME_PRODUCT) as ProductSearchModel
            name = productSearchModel.name
            category = productSearchModel.category
        }

        val q = QProduct.product
        var s = q.isNotNull

        if (StringUtils.isNotEmpty(name)) {
            s = s.and(q.name.equalsIgnoreCase(name))
        }

        if (status != null) {
            s = s.and(q.status.eq(status))
        }

        if (StringUtils.isNotEmpty(category)) {
            s = s.and(q.productCategories.contains(findProductCategoryByName(category!!)))
        }

        return s

    }

}