package com.samaxxw.app.interceptor


import com.samaxxw.model.Permission
import com.samaxxw.model.Role
import com.samaxxw.model.RolePermissionMap
import com.samaxxw.service.*
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.dao.EmptyResultDataAccessException
import org.springframework.lang.Nullable
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Component
import org.springframework.web.servlet.ModelAndView
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter
import org.springframework.web.util.UriUtils
import java.nio.file.Paths
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Component
class GrantProductResourcesAccessInterceptor : HandlerInterceptorAdapter() {

    val log = LoggerFactory.getLogger(this.javaClass)!!

    @Autowired
    private val rolePermissionMapService : RolePermissionMapService? = null

    @Autowired
    private val roleService : RoleService? = null

    @Autowired
    val salesTrxService : SalesTrxService? = null

    @Autowired
    val productResourcesService: ProductResourcesService? = null

    @Autowired
    val userService: UserService? = null

    @Throws(Exception::class)
    override fun preHandle(request: HttpServletRequest, response: HttpServletResponse, handler: Any): Boolean {
        val startTime = System.currentTimeMillis()
        log.debug("-------- GrantProductResourcesAccessInterceptor.preHandle --- ")
        log.debug("Request URL: " + request.requestURL)
        log.debug("Request URI: " + request.requestURI)
        log.debug("Start Time: " + System.currentTimeMillis())

        request.setAttribute("startTime", startTime)

        // ## Security Header Check begin ##

        val handlerResult = true
        val requestURI = request.servletPath
        val logPrefix = "FROM : " + request.remoteAddr + "- preHandle(): "
        val requestPermission = Permission()
        val session = request.session

        val user = userService?.currentUser()
        // Get requested URI
        // Use themethod as reference
        // Get user sales record
        // Get product resources URL

        // Compare the the product resources URL is contain /themethod/
        // If true -> permit

        // Get first segment of uri

        val path = Paths.get(requestURI)
        val segmentList = path.toMutableList()

        val firstSegment = path.first()
        val languageSegment = segmentList[1]

        val productResource = productResourcesService?.findByResource("${firstSegment}/${languageSegment}")

        val isUserOwnedProduct = salesTrxService?.checkSaleRecordExistingByUserIdAndProductId(user!!.id!!, productResource!!.product!!.id!!)

        if (isUserOwnedProduct!!) {
            log.info("[Product - ${productResource!!.product!!.name}] Product Resources Access Permitted to ${user!!.email}")
            return true
        }

        // User Security context
        val authentication = SecurityContextHolder.getContext().authentication
        val authorities : MutableCollection<out GrantedAuthority>? = authentication.authorities


        requestPermission.url = requestURI

        // TODO check with override user role like ROLE_ADMIN
        // Get current role permission map
        // Loop current user authorities
        authorities!!.forEach { authority ->

            if ("ROLE_DISABLED" == authority.authority) {
                log.info("Access Denied for Disabled User : ${user!!.email}")
                response.sendRedirect(request.contextPath + "/accessDenied") // Redirect to access denied page
                return false
            }

            // Admin can override all the product resource permission
            if ("ROLE_ADMIN" == authority.authority) {
                log.info("[Product - ${productResource!!.product!!.name}] Admin : ${user!!.email} Override Access")
                return true
            }

        }

        log.info("[Product - ${productResource!!.product!!.name}] Product Resources Access Denied to ${user!!.email}")

        response.sendRedirect(request.contextPath + "/product/detail/${productResource.product!!.id!!}") // Redirect to product detail page

        return false

    }

    @Throws(Exception::class)
    override fun postHandle(request: HttpServletRequest, response: HttpServletResponse, handler: Any, @Nullable modelAndView: ModelAndView?) {

        log.debug("-------- LogInterception.postHandle --- ")
        log.debug("Request URL: " + request.requestURL)

        // You can add attributes in the modelAndView
        // and use that in the view page
    }

    @Throws(Exception::class)
    override fun afterCompletion(request: HttpServletRequest, response: HttpServletResponse, handler: Any, @Nullable ex: java.lang.Exception?) {
        log.debug("-------- LogInterception.afterCompletion --- ")

        val startTime = request.getAttribute("startTime") as Long
        val endTime = System.currentTimeMillis()
        log.debug("Request URL: " + request.requestURL)
        log.debug("End Time: $endTime")

        log.debug("Time Taken: " + (endTime - startTime))
    }

}