package com.samaxxw.app.config

import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.stereotype.Component


@Component
class CaptchaSettings {

    @Value("\${google.recaptcha-key.site}")
    val site: String? = null

    @Value("\${google.recaptcha-key.secret}")
    val secret: String? = null

}