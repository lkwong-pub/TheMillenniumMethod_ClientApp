package com.samaxxw.app.config

import com.samaxxw.app.interceptor.GrantProductResourcesAccessInterceptor
import com.samaxxw.app.interceptor.SecurityInterceptor
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean
import org.springframework.boot.web.servlet.FilterRegistrationBean
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.core.Ordered
import org.springframework.web.servlet.config.annotation.InterceptorRegistry
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter


@Configuration
class WebMvcConfig : WebMvcConfigurerAdapter() {

//    @Value("\${azure.application-insights.instrumentation-key}")
//    val instrumentationKey: String? = null

    @Bean
    fun securityInterceptor(): SecurityInterceptor {
        return SecurityInterceptor()
    }

    @Bean
    fun grantProductResourcesAccessInterceptor(): GrantProductResourcesAccessInterceptor {
        return GrantProductResourcesAccessInterceptor()
    }

    //
    override fun addInterceptors(registry: InterceptorRegistry) {
        // LogInterceptor apply to all URLs.
        registry.addInterceptor(securityInterceptor())
                .excludePathPatterns("/healthCheck**", "/registration", "/login", "/error", "/accessDenied")
                .excludePathPatterns("/webjars/**", "/css/**", "/js/**", "/images/**", "/nav/**") // static resources
                .excludePathPatterns("/themethod/**") // Checked by GrantProductResourcesAccessInterceptor
                .excludePathPatterns("/user/verification/**", "/logout/**", "/user/resetPassword/**")

        registry.addInterceptor(grantProductResourcesAccessInterceptor())
                .addPathPatterns("/themethod/**")


//        // Old Login url, no longer use.
//        // Use OldURLInterceptor to redirect to a new URL.
//        registry.addInterceptor(OldLoginInterceptor())//
//                .addPathPatterns("/admin/oldLogin")

//        // This interceptor apply to URL like /admin/*
//        // Exclude /admin/oldLogin
//        registry.addInterceptor(AdminInterceptor())//
//                .addPathPatterns("/admin/*")//
//                .excludePathPatterns("/admin/oldLogin")
    }

    //Initialize AI TelemetryConfiguration via Spring Beans
//    @Bean
//    fun telemetryConfig(): String? {
//        val telemetryKey = instrumentationKey
//        if (telemetryKey != null) {
//            TelemetryConfiguration.getActive().instrumentationKey = telemetryKey
//        }
//        return telemetryKey
//    }

    /**
     * Programmatically registers a FilterRegistrationBean to register WebRequestTrackingFilter
     * @param webRequestTrackingFilter
     * @return Bean of type [FilterRegistrationBean]
     */
//    @Bean
//    fun webRequestTrackingFilterRegistrationBean(webRequestTrackingFilter: WebRequestTrackingFilter): FilterRegistrationBean<*> {
//        val registration = FilterRegistrationBean<WebRequestTrackingFilter>()
//        registration.filter = webRequestTrackingFilter
//        registration.addUrlPatterns("/**")
//        // TODO have to filter front-end stuff
//        registration.order = Ordered.HIGHEST_PRECEDENCE + 10
//        return registration
//    }


    /**
     * Creates bean of type WebRequestTrackingFilter for request tracking
     * @param applicationName Name of the application to bind filter to
     * @return [Bean] of type [WebRequestTrackingFilter]
     */
//    @Bean
//    @ConditionalOnMissingBean
//    fun webRequestTrackingFilter(@Value("\${application.name}") applicationName: String): WebRequestTrackingFilter {
//        return WebRequestTrackingFilter(applicationName)
//    }

}