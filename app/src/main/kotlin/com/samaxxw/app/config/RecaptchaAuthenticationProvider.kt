package com.samaxxw.app.config

import com.samaxxw.app.service.impl.CaptchaValidator
import com.samaxxw.exception.SystemErrorException
import com.samaxxw.exception.ValidationException
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.authentication.AuthenticationProvider
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.core.AuthenticationException
import org.springframework.stereotype.Component
import javax.servlet.http.HttpServletRequest


@Component
class RecaptchaAuthenticationProvider : AuthenticationProvider {

    @Autowired
    val httpServletRequest: HttpServletRequest? = null

    @Autowired
    val captchaValidator: CaptchaValidator? = null

    companion object {
        val log = LoggerFactory.getLogger(this.javaClass)!!
    }

    @Throws(AuthenticationException::class)
    override fun authenticate(authentication: Authentication): Authentication? {

        val token = httpServletRequest!!.getParameter("token")

        if (!captchaValidator?.validateCaptcha(token)!!) {
            val msg = "Potential BOT detected :: Login by [${authentication.name}] from [${httpServletRequest!!.remoteAddr}]"
            throw SystemErrorException(msg)
        }

        return null
//        return if (shouldAuthenticateAgainstThirdPartySystem()) {
//        return if (true) {
//            // use the credentials
//            // and authenticate against the third-party system
//            UsernamePasswordAuthenticationToken(
//                    name, password, ArrayList<GrantedAuthority>())
//        } else {
//            null
//        }
    }

    override fun supports(authentication: Class<*>): Boolean {
        return authentication == UsernamePasswordAuthenticationToken::class.java
    }
}