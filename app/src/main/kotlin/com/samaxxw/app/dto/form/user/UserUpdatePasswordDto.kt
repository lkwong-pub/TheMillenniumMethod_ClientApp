package com.samaxxw.app.dto.form.user


import com.samaxxw.constraint.FieldMatch
import com.samaxxw.constraint.FieldMatchList
import java.io.Serializable
import javax.validation.constraints.AssertTrue
import javax.validation.constraints.Email
import javax.validation.constraints.NotEmpty

@FieldMatchList(
        FieldMatch(first = "password", second = "confirmPassword", message = "The password fields must match be matched"))
class UserUpdatePasswordDto : Serializable {

    @NotEmpty
    var password: String? = null

    @NotEmpty
    var confirmPassword: String? = null

}
