package com.samaxxw.app.dto.form.user


import java.io.Serializable
import javax.validation.Valid
import javax.validation.constraints.Email
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.NotNull

class UserRequestResetPasswordForm : Serializable {

    @NotEmpty
    @Email
    var email: String? = null

}
