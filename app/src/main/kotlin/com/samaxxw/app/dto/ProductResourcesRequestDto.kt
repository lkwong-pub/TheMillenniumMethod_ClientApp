package com.samaxxw.app.dto


import java.io.Serializable
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.NotNull

class ProductResourcesRequestDto : Serializable {

    @NotNull
    var userId: Long? = null

    @NotEmpty
    var permissionCode: String? = null

}
