package com.samaxxw.app.dto

import com.samaxxw.dto.model.ProductDto
import java.io.Serializable
import java.math.BigDecimal

class CheckoutItemDto : Serializable {

    var salesTrxId: Long? = null

    var productDto: ProductDto? = null

    var actualPrice: BigDecimal? = null

    constructor()


    constructor(productDto: ProductDto?, actualPrice: BigDecimal?) {
        this.productDto = productDto
        this.actualPrice = actualPrice
    }

}