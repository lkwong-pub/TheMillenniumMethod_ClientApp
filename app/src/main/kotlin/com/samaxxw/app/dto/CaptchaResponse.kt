package com.samaxxw.app.dto

import com.fasterxml.jackson.annotation.JsonProperty
import java.util.*


class CaptchaResponse {

    val success: Boolean? = null
    val timestamp: Date? = null
    val hostname: String? = null
    @JsonProperty("error-codes")
    val errorCodes: List<String>? = null

}