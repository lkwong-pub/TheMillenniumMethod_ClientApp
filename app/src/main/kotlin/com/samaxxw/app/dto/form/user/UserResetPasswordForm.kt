package com.samaxxw.app.dto.form.user


import java.io.Serializable
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.NotNull


class UserResetPasswordForm : Serializable {

    @NotNull
    var userId: Long? = null

    @NotEmpty
    var requestId: String? = null

    @NotEmpty
    var password: String? = null

    @NotEmpty
    var confirmPassword: String? = null

}
