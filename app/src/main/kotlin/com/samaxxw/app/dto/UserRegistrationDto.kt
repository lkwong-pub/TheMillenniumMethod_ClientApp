package com.samaxxw.app.dto


import com.samaxxw.constraint.FieldMatch
import com.samaxxw.constraint.FieldMatchList
import java.io.Serializable
import javax.validation.constraints.AssertTrue
import javax.validation.constraints.Email
import javax.validation.constraints.NotEmpty

@FieldMatchList(
        FieldMatch(first = "password", second = "confirmPassword", message = "The password fields must match"),
        FieldMatch(first = "email", second = "confirmEmail", message = "The email fields must match"))
class UserRegistrationDto : Serializable {

    @NotEmpty
    var firstName: String? = null

    @NotEmpty
    var lastName: String? = null

    @NotEmpty
    var password: String? = null

    @NotEmpty
    var confirmPassword: String? = null

    @Email
    @NotEmpty
    var email: String? = null

    @Email
    @NotEmpty
    var confirmEmail: String? = null

    @AssertTrue
    var terms: Boolean? = null

    @NotEmpty
    var token: String? = null

}
