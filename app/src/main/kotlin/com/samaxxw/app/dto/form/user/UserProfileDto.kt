package com.samaxxw.app.dto.form.user


import com.samaxxw.constraint.FieldMatch
import com.samaxxw.constraint.FieldMatchList
import java.io.Serializable
import javax.validation.constraints.AssertTrue
import javax.validation.constraints.Email
import javax.validation.constraints.NotEmpty

@FieldMatchList(
        FieldMatch(first = "email", second = "confirmEmail", message = "The email fields must match be matched"))
class UserProfileDto : Serializable {

    @NotEmpty
    var firstName: String? = null

    @NotEmpty
    var lastName: String? = null

    @Email
    @NotEmpty
    var email: String? = null

    @Email
    @NotEmpty
    var confirmEmail: String? = null

}
