package com.samaxxw.app.dto

import java.io.Serializable

class LandingPageUserProductResponse : Serializable {

    var productName: String? = null

    var purchasedDate: String? = null

}