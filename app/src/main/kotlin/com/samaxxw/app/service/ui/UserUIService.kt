package com.samaxxw.app.service.ui

import com.samaxxw.app.service.UserAccountService
import com.samaxxw.controller.AdvancedBaseRestController
import com.samaxxw.controller.AdvancedBaseRestController.Companion.PAGE_HEADER
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.ui.Model

@Service
class UserUIService {

    @Autowired
    val userAccountService : UserAccountService? = null

    // User Profile Page
    fun userProfileModel(model: Model) {
        model.addAttribute(AdvancedBaseRestController.PAGE_HEADER, "User Profile")
    }

    fun refreshProfileModel(model: Model) {
        userProfileModel(model)
        model.addAttribute("userProfile", userAccountService?.getUserProfileDto())
    }

    fun updatePasswordModel(model: Model) {
        model.addAttribute(PAGE_HEADER, "Update Password")
    }


}