package com.samaxxw.app.service

import com.samaxxw.dto.model.ProductResourceDto

interface ProductResourceRequestService {

    fun getProductResource(userId: Long, productId: Long): MutableList<ProductResourceDto>

    fun checkUserOwnedProduct(userId: Long, productId: Long) : Boolean

}