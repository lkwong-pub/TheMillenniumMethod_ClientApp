package com.samaxxw.app.service.ui

import com.samaxxw.app.controller.ProductClientController
import com.samaxxw.controller.template.BreadcrumbListDto
import com.samaxxw.service.ui.AbstractBreadCrumbService
import org.springframework.stereotype.Service

@Service
class BreadCrumbService : AbstractBreadCrumbService() {

    /**
     * Product Module
     */

    fun getProductRootBreadCrumb() : BreadcrumbListDto {
        return getProductRootBreadCrumb(false)
    }
    fun getProductRootBreadCrumb(isCurrentPage: Boolean) : BreadcrumbListDto {
        val breadcrumb = getRootUrl()
        if (isCurrentPage) {
            breadcrumb.breadcrumbList!!.add(BreadcrumbListDto.Breadcrumb("Product", null, true))
        } else {
            breadcrumb.breadcrumbList!!.add(BreadcrumbListDto.Breadcrumb("Product", "${ProductClientController.ROOT}/list", false))
        }
        return breadcrumb
    }

    fun getProductDetailBreadCrumb() : BreadcrumbListDto {
        return getProductDetailBreadCrumb(false, null)
    }

    fun getProductDetailBreadCrumb(isCurrentPage: Boolean, productId: Long?) : BreadcrumbListDto {
        val breadcrumb = getProductRootBreadCrumb()
        if (isCurrentPage) {
            breadcrumb.breadcrumbList!!.add(BreadcrumbListDto.Breadcrumb("Product Detail", null, true))
        } else {
            breadcrumb.breadcrumbList!!.add(BreadcrumbListDto.Breadcrumb("Product Detail", "${ProductClientController.ROOT}/detail/$productId", false))
        }

        return breadcrumb
    }

    /**
     * Checkout
     */

    fun getCheckoutRootBreadCrumb(productId: Long) : BreadcrumbListDto {
        val breadcrumb = getProductDetailBreadCrumb(false, productId)
        breadcrumb.breadcrumbList!!.add(BreadcrumbListDto.Breadcrumb("Checkout", null, true))
        return breadcrumb
    }

    /**
     * Transaction Record
     */

    fun getTransactionRecordRootBreadCrumb() : BreadcrumbListDto {
        val breadcrumb = getRootUrl()
        breadcrumb.breadcrumbList!!.add(BreadcrumbListDto.Breadcrumb("Transaction Record", null, true))
        return breadcrumb
    }

    /**
     * Purchased Item
     */

    fun getPurchasedItemRootBreadCrumb() : BreadcrumbListDto {
        val breadcrumb = getRootUrl()
        breadcrumb.breadcrumbList!!.add(BreadcrumbListDto.Breadcrumb("Purchased Item", null, true))
        return breadcrumb
    }

    /**
     * Update Password
     */

    fun getUpdatePasswordRootBreadCrumb() : BreadcrumbListDto {
        val breadcrumb = getRootUrl()
        breadcrumb.breadcrumbList!!.add(BreadcrumbListDto.Breadcrumb("Update Password", null, true))
        return breadcrumb
    }

    /**
     * Update Password
     */

    fun getUserProfileRootBreadCrumb() : BreadcrumbListDto {
        val breadcrumb = getRootUrl()
        breadcrumb.breadcrumbList!!.add(BreadcrumbListDto.Breadcrumb("User Profile", null, true))
        return breadcrumb
    }


}