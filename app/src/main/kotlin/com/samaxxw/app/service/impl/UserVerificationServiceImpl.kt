package com.samaxxw.app.service.impl

import com.samaxxw.dao.UserDao
import com.samaxxw.dao.UserVerificationDao
import com.samaxxw.exception.ResourceNotFoundException
import com.samaxxw.exception.SystemErrorException
import com.samaxxw.app.service.UserVerificationService
import com.samaxxw.model.QUserVerification.userVerification
import com.samaxxw.model.User
import com.samaxxw.model.UserVerification
import com.samaxxw.service.generator.UserVerificationCodeGenerator
import com.samaxxw.util.UserStatusEnum
import com.samaxxw.util.UserVerificationStatusEnum
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional


@Service
@Transactional
class UserVerificationServiceImpl : UserVerificationService {

    val log = LoggerFactory.getLogger(this.javaClass)!!

    @Autowired
    val userDao: UserDao? = null

    @Autowired
    val userVerificationDao: UserVerificationDao? = null

    @Autowired
    val userVerificationCodeGenerator : UserVerificationCodeGenerator? = null

    /**
     * Return true - verify success
     */
    override fun checkVerificationCode(userId: Long, verificationCode: String) {

        // Get user
        val user = userDao?.findOne(userId)
        if (user == null) {
            throw ResourceNotFoundException("[User Verification] Verification Error - User ID <$userId> not found")
        }
        // Get user verification
        val userVerification = userVerificationDao?.findByUserAndVerificationCode(user, verificationCode)
        if (userVerification == null) {
            log.error("[User Verification] Verification Error - Verification Code <$verificationCode> with User ID <$userId> not found")
            throw ResourceNotFoundException("[User Verification] Verification Error - Invalid Verification Code")
        }

        if (UserVerificationStatusEnum.VERIFIED.status == userVerification.status) {
            log.error("[User Verification] Verification Error - Verification Code <$verificationCode> with User ID <$userId> has been verified")
            throw SystemErrorException("[User Verification] Verification Error - Verification Code has been verified")
        }

        if (UserVerificationStatusEnum.DISABLE.status == userVerification.status) {
            log.error("[User Verification] Verification Error - Verification Code <$verificationCode> with User ID <$userId> has been expired")
            throw SystemErrorException("[User Verification] Verification Error - Verification Code has been expired")
        }

        if (UserVerificationStatusEnum.ACTIVE.status == userVerification.status) {
            // update verification status
            userVerification.status = UserVerificationStatusEnum.VERIFIED.status

            // update user status
            user.status = UserStatusEnum.ACTIVE.status

            userDao?.save(user)
            userVerificationDao?.save(userVerification)
        } else {
            throw SystemErrorException("[User Verification] Verification Error - Verification Code <$verificationCode> with User ID <$userId> unhandled error")
        }

    }

    override fun createUserVerification(currentUser: User) : UserVerification {

        disableExpiredUserVerification(currentUser) // Disable expired user verification record

        val userVerification = UserVerification()
        userVerification.user = currentUser
        userVerification.verificationCode = userVerificationCodeGenerator?.newNumber()
        userVerification.status = UserVerificationStatusEnum.ACTIVE.status

        return userVerificationDao?.save(userVerification)!!
    }

    /**
     * Disable / Revoke expired user verification record
     */
    override fun disableExpiredUserVerification(currentUser: User) {

        val userVerificationList = userVerificationDao?.findByUserAndStatus(currentUser, UserVerificationStatusEnum.ACTIVE.status)

        if (userVerificationList!!.isNotEmpty()) {

            userVerificationList.forEach {
                userVerification ->
                run {
                    userVerification.status = UserVerificationStatusEnum.DISABLE.status
                    log.info("[Disable User Verification] ID : ${userVerification.id}")
                    userVerificationDao?.save(userVerification)
                }
            }
        }

    }

}
