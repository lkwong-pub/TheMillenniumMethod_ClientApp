package com.samaxxw.app.service

import com.samaxxw.dto.model.SalesTrxDto
import com.samaxxw.dto.paged.PagedDto
import org.springframework.beans.factory.annotation.Qualifier
import javax.servlet.http.HttpServletRequest

@Qualifier("UserPurchaseHistoryService")
interface UserPurchaseHistoryService {

    fun getPurchaseProductList(page: Int?, per_page: Int?, sortField: String, direction: String, request: HttpServletRequest): PagedDto<SalesTrxDto>

}