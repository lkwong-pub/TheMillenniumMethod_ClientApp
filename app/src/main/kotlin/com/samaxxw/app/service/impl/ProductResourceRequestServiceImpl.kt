package com.samaxxw.app.service.impl

import com.samaxxw.app.dto.UserRegistrationDto
import com.samaxxw.app.exception.ProductResourcesAccessDeniedException
import com.samaxxw.app.service.ProductResourceRequestService
import com.samaxxw.app.service.UserAccountService
import com.samaxxw.dto.model.ProductResourceDto
import com.samaxxw.mapper.ProductResourcesMapper
import com.samaxxw.model.ProductResource
import com.samaxxw.model.User
import com.samaxxw.repository.RoleRepository
import com.samaxxw.repository.UserRepository
import com.samaxxw.service.ProductResourcesService
import com.samaxxw.service.SalesTrxService
import com.samaxxw.util.ProductResourcesStatusEnum
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional


@Service
@Transactional
class ProductResourceRequestServiceImpl : ProductResourceRequestService {

    @Autowired
    val salesTrxService: SalesTrxService? = null

    @Autowired
    val productResourcesService: ProductResourcesService? = null

    /**
     * Get Product Resource with Sales Record Checking
     */
    @Transactional
    override fun getProductResource(userId: Long, productId: Long): MutableList<ProductResourceDto> {

        var sales = salesTrxService?.getSaleRecordExistingByUserIdAndProductId(userId, productId)

        if (sales == null) {
            throw ProductResourcesAccessDeniedException()
        }

        var resourcesList = productResourcesService?.findByStatusAndProduct(
                ProductResourcesStatusEnum.PUBLISH.status,
                sales.product!!)

        return ProductResourcesMapper.INSTANCE.toDTOs(resourcesList!!)

    }

    @Transactional
    override fun checkUserOwnedProduct(userId: Long, productId: Long) : Boolean {
        return salesTrxService?.checkSaleRecordExistingByUserIdAndProductId(userId, productId)!!
    }


}
