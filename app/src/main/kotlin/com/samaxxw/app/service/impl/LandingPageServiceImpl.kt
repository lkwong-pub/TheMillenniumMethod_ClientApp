package com.samaxxw.app.service.impl

import com.samaxxw.app.dto.LandingPageUserProductResponse
import com.samaxxw.app.service.LandingPageService
import com.samaxxw.dao.ProductCategoryDao
import com.samaxxw.dao.ProductDao
import com.samaxxw.model.Product
import com.samaxxw.model.ProductCategory
import com.samaxxw.repository.ProductRepository
import com.samaxxw.service.SalesTrxService
import com.samaxxw.service.UserService
import com.samaxxw.util.SalesStatus
import org.apache.commons.collections.CollectionUtils
import org.apache.commons.lang3.time.DateFormatUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.util.*


@Service
@Transactional
class LandingPageServiceImpl : LandingPageService {

    @Autowired
    private val productRepository: ProductRepository? = null

    @Autowired
    private val productDao: ProductDao? = null

    @Autowired
    private val productCategoryDao: ProductCategoryDao? = null

    @Autowired
    private val salesTrxService: SalesTrxService? = null

    @Autowired
    private val userService: UserService? = null

    fun findByCategoryName(categoryName : String) : ProductCategory {
        return productCategoryDao?.findByName(categoryName)!!
    }

    fun findByProductName(productName: String) : Product? {
        return productRepository?.findByName(productName)
    }


    override fun findTop10ByStatusOrderByCreateDatetimeDesc(status: Int) : MutableList<Product>? {
        return productDao?.findTop10ByStatusOrderByCreateDatetimeDesc(status)
    }

    override fun getLandingPageProduct() : TreeMap<Int, LandingPageUserProductResponse> {
        val currentUser = userService?.currentUser()
        val salesTrxList = salesTrxService?.getSalesTrxByUser(currentUser!!)

        val result = TreeMap<Int, LandingPageUserProductResponse>()
        if (CollectionUtils.isNotEmpty(salesTrxList)) {

            salesTrxList!!.forEach { salesTrx ->

                if (CollectionUtils.isNotEmpty(salesTrx.salesList)) {

                    salesTrx.salesList!!.forEach {
                        val obj = LandingPageUserProductResponse()
                        obj.productName = it.product!!.name
                        obj.purchasedDate = DateFormatUtils.format(it.createDatetime, "yyyy-MM-dd")
                        if (it.status == SalesStatus.ACTIVE.status) {
                            result[it.product!!.id!!.toInt()] = obj
                        }
                    }

                }

            }

        }

        return result

    }


}
