package com.samaxxw.app.service.impl

import com.samaxxw.app.config.CaptchaSettings
import com.samaxxw.app.dto.CaptchaResponse
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.util.LinkedMultiValueMap
import org.springframework.web.client.RestTemplate


@Service
class CaptchaValidator {

    @Autowired
    val captchaSettings: CaptchaSettings? = null

    companion object {
        val log = LoggerFactory.getLogger(this.javaClass)!!
        private val GOOGLE_RECAPTCHA_ENDPOINT = "https://www.google.com/recaptcha/api/siteverify"
    }


    fun validateCaptcha(captchaResponse: String): Boolean {

        val requestMap = LinkedMultiValueMap<Any, Any>()
        requestMap.add("secret", captchaSettings!!.secret)
        requestMap.add("response", captchaResponse)

        val apiResponse = RestTemplate().postForObject(GOOGLE_RECAPTCHA_ENDPOINT, requestMap, CaptchaResponse::class.java)

        if (apiResponse!!.success!!) {
            return java.lang.Boolean.TRUE == apiResponse.success
        } else {
            log.error("Failed to pass recaptcha validation.")
            apiResponse.errorCodes!!.forEach {
                log.error(it)
            }
            return false
        }

    }


}