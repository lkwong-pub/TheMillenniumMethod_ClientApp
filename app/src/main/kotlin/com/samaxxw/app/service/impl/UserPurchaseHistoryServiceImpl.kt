package com.samaxxw.app.service.impl

import com.querydsl.core.types.Predicate
import com.samaxxw.app.controller.searchModel.UserPurchaseHistorySearchModel
import com.samaxxw.app.service.UserPurchaseHistoryService
import com.samaxxw.controller.searchModel.SearchModelName
import com.samaxxw.dao.BaseDao
import com.samaxxw.dao.SalesTrxDao
import com.samaxxw.dto.create.CreateSalesTrxDto
import com.samaxxw.dto.create.UpdateSalesTrxDto
import com.samaxxw.dto.model.SalesTrxDto
import com.samaxxw.dto.paged.PagedDto
import com.samaxxw.dtoservice.impl.AdvancedBaseDtoServiceImpl
import com.samaxxw.dtoservice.impl.SalesTrxDtoServiceImpl
import com.samaxxw.mapper.SalesTrxMapper
import com.samaxxw.mapper.core.DtoMapper
import com.samaxxw.model.QSalesTrx
import com.samaxxw.model.SalesTrx
import com.samaxxw.service.impl.SalesTrxServiceImpl
import com.samaxxw.util.SalesStatus
import org.apache.commons.lang3.StringUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Pageable
import org.springframework.data.domain.Sort
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import javax.servlet.http.HttpServletRequest


@Service
@Transactional
@Qualifier("UserPurchaseHistoryService")
class UserPurchaseHistoryServiceImpl : UserPurchaseHistoryService, SalesTrxDtoServiceImpl() {

    /**
     * Get User purchased product list
     */
    @Transactional
    override fun getPurchaseProductList(page: Int?, per_page: Int?, sortField: String, direction: String, request: HttpServletRequest): PagedDto<SalesTrxDto> {
        return super.findAll(getPredicate(request), getPageable(page, per_page, sortField, direction), "page", "per_page", "sort", sortField)
    }

    /**
     * Get User purchased transaction list
     */



    override fun getPredicate(request: HttpServletRequest): Predicate {

        // Get Parameter directly from session
        var salesTrxSearchModel: UserPurchaseHistorySearchModel?

        var email : String? = ""

        var trxReferenceCode : String? = ""

        var userId : Long? = null

        var productName : String? = null

        var status : Int? = null

        var payId : String? = null

        if (request.session.getAttribute(SearchModelName.SEARCH_FILTER_NAME_SALES_TRX) != null) {
            salesTrxSearchModel = request.session.getAttribute(SearchModelName.SEARCH_FILTER_NAME_SALES_TRX) as UserPurchaseHistorySearchModel
            email = salesTrxSearchModel.email
            userId = salesTrxSearchModel.userId
            trxReferenceCode = salesTrxSearchModel.trxReferenceCode
            productName = salesTrxSearchModel.productName
            status = SalesStatus.ACTIVE.status
            payId = salesTrxSearchModel.payId
        }

        val q = QSalesTrx.salesTrx
        var s = q.isNotNull()

        if (userId != null) {
            s = s.and(q.user.id.eq(userId))
        }

        if (StringUtils.isNotEmpty(email)) {
            s = s.and(q.user.email.equalsIgnoreCase(email))
        }

        if (StringUtils.isNotBlank(trxReferenceCode)) {
            s = s.and(q.trxReferenceCode.equalsIgnoreCase(trxReferenceCode))
        }

        if (StringUtils.isNotBlank(productName)) {
            s = s.and(q.salesList.any().product.name.equalsIgnoreCase(productName))
        }

        if (StringUtils.isNotBlank(payId)) {
            s = s.and(q.payId.equalsIgnoreCase(payId))
        }

        if (status != null) {
            s = s.and(q.status.eq(status))
        }

        return s

    }

}
