package com.samaxxw.app.service.impl

import com.samaxxw.app.config.SessionKey
import com.samaxxw.app.dto.CheckoutItemDto
import com.samaxxw.app.service.CheckoutService
import com.samaxxw.dao.*
import com.samaxxw.dto.domain.PayPalPaymentRequest
import com.samaxxw.dto.model.ProductDto
import com.samaxxw.dtoservice.ProductDtoService
import com.samaxxw.exception.ResourceNotFoundException
import com.samaxxw.exception.ValidationException
import com.samaxxw.mapper.ProductMapper
import com.samaxxw.model.*
import com.samaxxw.service.MailService
import com.samaxxw.service.PayPalService
import com.samaxxw.service.SalesTrxService
import com.samaxxw.service.generator.ReferenceCodeGenerator
import com.samaxxw.util.ProductStatusEnum
import com.samaxxw.util.SalesStatus
import com.samaxxw.util.UserStatusEnum
import org.apache.commons.lang3.StringUtils
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.beans.factory.annotation.Value
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.math.BigDecimal
import javax.servlet.http.HttpServletRequest


@Service
@Transactional
class CheckoutServiceImpl : CheckoutService {

    val log = LoggerFactory.getLogger(this.javaClass)

    @Autowired
    val userDao: UserDao? = null

    @Autowired
    val salesTrxDao: SalesTrxDao? = null

    @Autowired
    val salesDao: SalesDao? = null

    @Autowired
    val salesTrxService: SalesTrxService? = null

    @Qualifier("productClientDtoService")
    @Autowired
    val productDtoService: ProductDtoService? = null

    @Autowired
    val productDao: ProductDao? = null

    @Autowired
    var paypalService: PayPalService? = null

    @Autowired
    val httpServletRequest: HttpServletRequest? = null

    @Autowired
    val referenceCodeGenerator: ReferenceCodeGenerator? = null

    @Autowired
    val salesTrxLogDao: SalesTrxLogDao? = null

    @Autowired
    val mailService: MailService? = null

    @Value("\${app.email-header-prefix}")
    val emailHeaderPrefix : String? = null

    @Value("\${paypal.returnUrl}")
    val paypalReturnUrl : String? = null

    override fun validateCheckoutItem(productId: Long): CheckoutItemDto {
        var currentUser = getUserDetail()
        val user = getUser(currentUser.username)
        val userOwnedProduct = checkCurrentUserSaleRecord(user.id!!, productId)
        val product = getProduct(productId)

        if (!ProductStatusEnum.PUBLISH.status.equals(product.status!!)) {
            throw ValidationException("", "", "Product is not available")
        }
        if (userOwnedProduct) {
            throw ValidationException("", "", "You have owned this product")
        }

        checkUserStatus(user)

        return CheckoutItemDto(ProductMapper.INSTANCE.toDTO(product), productDtoService?.calculatePriceWithDiscount(product.price!!, product.discount!!))

    }

    override fun checkoutProcess(): Map<String, Any> {

        var checkoutItem = httpServletRequest!!.session.getAttribute(SessionKey.SESSION_CHECKOUT) as CheckoutItemDto

        // Get product
        val product = getProduct(checkoutItem.productDto!!.id!!)

        // Get User
        val user = getUser(getUserDetail().username)

        // Check user status
        checkUserStatus(user)

        if (checkoutItem.actualPrice != null) {

            var requestId : String

            if (StringUtils.isNotBlank(product.abbreviation)) {
                requestId = "${product.abbreviation}-${referenceCodeGenerator?.newNumber()!!}"
            } else {
                requestId = referenceCodeGenerator?.newNumber()!!
            }

            log.info("[Request Id : $requestId | PayPal payment request] - \n User : ${getUserDetail().username} \n Checkout Item : ${checkoutItem.productDto!!.name} \n Price : ${checkoutItem.actualPrice}")

            // Persist sales
            val sales = persistSales(product)

            // Persist saleTrx
            val salesTrx = persistSalesTrx(user, sales.salesAmount!!, mutableSetOf(sales), requestId)

            // Persist sale trx log

            try {

                // Call PayPal
                val payPalPaymentRequest = PayPalPaymentRequest(checkoutItem.actualPrice.toString(), checkoutItem.productDto!!.name!!, paypalReturnUrl)

                val result: Map<String, Any> = paypalService?.createPayment(payPalPaymentRequest)!!

                persistSalesTrxLog("[Request Id : ${salesTrx.trxReferenceCode}] Create Payment", salesTrx)

                // save sales trx id to session for complete process
                checkoutItem.salesTrxId = salesTrx.id

                httpServletRequest!!.session.setAttribute(SessionKey.SESSION_CHECKOUT, checkoutItem)

                return result

            } catch (e: Exception) {
                val errMsg = "Failed to call PayPal service"
                log.error("[PayPal payment request] - $errMsg")
            }


        }

        val errMsg = "Failed to check-out"
        log.error("[PayPal payment request] - $errMsg")
        throw RuntimeException(errMsg)
    }

    override fun completeCheckout() {

        val response = paypalService?.completePayment(httpServletRequest!!)
        var checkoutItem = httpServletRequest!!.session.getAttribute(SessionKey.SESSION_CHECKOUT) as CheckoutItemDto

        if (response!!["status"]!! == "success") {

            // Mark
            confirmSalesTrxPayment(checkoutItem.salesTrxId!!, response["payId"]!!.toString())

        } else {

            // Payment process Failed
            log.error("[Complete Checkout - SalesTrxId : ${checkoutItem.salesTrxId}] Failed to complete payment")

        }


    }

    fun checkUserStatus(user: User) {
        if (UserStatusEnum.VERIFYING.status == user.status) {
            throw ValidationException("", "", "The account have to be verified before checkout.")
        } else if (UserStatusEnum.DISABLE.status == user.status) {
            throw ValidationException("", "", "Account disabled.") // Assume this case has been blocked by security interceptor
        } else {
            return
        }

    }


    fun checkCurrentUserSaleRecord(userId: Long, productId: Long): Boolean {
        return salesTrxService?.checkSaleRecordExistingByUserIdAndProductId(userId, productId)!!
    }

    fun getUserDetail(): UserDetails {
        return SecurityContextHolder.getContext().authentication.principal as UserDetails
    }

    fun getSalesTrxByUser(user: User): MutableList<SalesTrx>? {
        return salesTrxDao?.findByUser(user)

    }

    fun getUser(email: String): User {
        return userDao?.findByEmail(email)!!
    }

    fun getProduct(id: Long): Product {
        return productDao?.findOne(id)!!
    }

    fun confirmSalesTrxPayment(salesTrxId: Long, payId: String) : SalesTrx {

        val salesTrx = salesTrxDao?.findOne(salesTrxId)

        if (salesTrx == null) {
            val errMsg = "[SaleTrxId - $salesTrxId] Record not found"
            log.error(errMsg)
            throw ResourceNotFoundException(errMsg)
        }

        salesTrx.salesList!!.forEach {
            sales ->
            run {
                sales.status = SalesStatus.ACTIVE.status
            }
        }

        sendCheckoutOrderEmail(salesTrx)
        salesTrx.status = SalesStatus.ACTIVE.status
        salesTrx.payId = payId

        return salesTrxDao?.save(salesTrx)!!

    }

    fun sendCheckoutOrderEmail(salesTrx: SalesTrx) {
        val header =
                "Hi! ${salesTrx.user!!.firstName + ' ' + salesTrx.user!!.lastName},  <br>" +
                        "<br>"+
                        "Thank You for purchasing from The Millennium Method. Please check the items below: <br>" +
                        "<br>"+
                        "<b>Reference Number</b> : ${salesTrx.trxReferenceCode}<br>"

        var itemsContent = "<b>Item</b> : <br>"

        salesTrx.salesList!!.forEach {
            sales ->
            run {
                itemsContent += "${sales.product!!.name}  ---- AUD$ ${sales.salesAmount}<br>"
            }
        }

        itemsContent += "Total : AUD$ ${salesTrx.totalSalesAmount} <br>" +
                            "<br>"

        val footer = "Best Regards, <br>" +
                        "The Millennium Method<br>"

        val content = header + itemsContent + footer

        try {
            mailService?.sendEmailMessage(salesTrx.user!!.email!!, "$emailHeaderPrefix Order Notification Ref : ${salesTrx.trxReferenceCode}", content)
        } catch (e: Exception) {
            e.printStackTrace()
            log.error("Error on sending checkout email message TrxId : ${salesTrx.trxReferenceCode}")
        }
    }

    /**
     * State inactive before payment completion
     */
    fun persistSales(product: Product): Sales {
        var sales = Sales()
        sales.product = product
//        sales.salesTrx = salesTrx
        sales.status = SalesStatus.INACTIVE.status
        sales.salesAmount = productDtoService?.calculatePriceWithDiscount(product.price!!, product.discount!!)
        return salesDao?.save(sales)!!
    }

    /**
     * State inactive before payment completion
     */
    fun persistSalesTrx(user: User, totalAmount: BigDecimal, sales: MutableSet<Sales>, requestId: String): SalesTrx {
        var salesTrx = SalesTrx()
        salesTrx.description = ""
        salesTrx.remarks = "Normal purchased by " + getUserDetail().username
        salesTrx.user = user
        salesTrx.trxReferenceCode = requestId
        salesTrx.status = SalesStatus.INACTIVE.status
        salesTrx.totalSalesAmount = totalAmount
        salesTrx.salesList = sales
        return salesTrxDao?.save(salesTrx)!!
    }

    fun persistSalesTrxLog(logMsg: String, salesTrx: SalesTrx): SalesTrxLog {
        var salesTrxLog = SalesTrxLog()
        salesTrxLog.description = logMsg
        salesTrxLog.salesTrx = salesTrx
        salesTrxLog.status = 0 // TODO use enum
        return salesTrxLogDao?.save(salesTrxLog)!!
    }

}
