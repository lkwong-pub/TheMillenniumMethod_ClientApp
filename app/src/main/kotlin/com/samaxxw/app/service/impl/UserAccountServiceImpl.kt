package com.samaxxw.app.service.impl

import com.samaxxw.app.dto.UserRegistrationDto
import com.samaxxw.app.dto.form.user.UserProfileDto
import com.samaxxw.app.dto.form.user.UserUpdatePasswordDto
import com.samaxxw.app.service.UserAccountService
import com.samaxxw.app.service.UserVerificationService
import com.samaxxw.exception.SuperAdminUserProtectException
import com.samaxxw.exception.SystemErrorException
import com.samaxxw.model.User
import com.samaxxw.model.UserVerification
import com.samaxxw.repository.RoleRepository
import com.samaxxw.repository.UserRepository
import com.samaxxw.service.MailService
import com.samaxxw.service.UserService
import com.samaxxw.util.UserStatusEnum
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.core.env.Environment
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.lang.Exception


@Service
@Transactional
class UserAccountServiceImpl : UserAccountService {

    @Autowired
    private val passwordEncoder: BCryptPasswordEncoder? = null

    @Autowired
    private val userRepository: UserRepository? = null

    @Autowired
    private val roleRepository: RoleRepository? = null

    @Autowired
    val userVerificationService : UserVerificationService? = null

    @Autowired
    val userService: UserService? = null

    @Autowired
    val mailService: MailService? = null

    @Value("\${app.verification-url-prefix}")
    val verificationUrlPrefix : String? = null

    @Value("\${app.email-header-prefix}")
    val emailHeaderPrefix : String? = null

    /**
     * For Registration
     */
    @Transactional
    override fun registration(registration: UserRegistrationDto): User {
        var user = User()
        user.firstName = registration.firstName
        user.lastName = registration.lastName
        user.email = registration.email
        user.password = passwordEncoder!!.encode(registration.password)
        user.roles = mutableListOf(roleRepository!!.findByName("ROLE_USER"))
        user.status = UserStatusEnum.VERIFYING.status

//        user.createDatetime = Date.from(LocalDateTime.now().toInstant(ZoneOffset.UTC))
        user = userRepository!!.save(user)
//        throw NullPointerException("test")

        val userVerification = createUserVerification(user)
        sendUserVerificationEmail(user, userVerification.verificationCode!!)

        return user
    }

    override fun getUserProfileDto(): UserProfileDto {

        var userProfielDto = UserProfileDto()
        val currentUser = userService?.currentUser()

        return convertUserProfileDto(currentUser!!)

    }

    @Transactional
    override fun updateUserProfile(userProfileDto: UserProfileDto, isEmailUpdate: Boolean) {

        val currentUser = userService?.currentUser()!!

        if (userService?.adminUserRestrictionCheck()!!) {
            throw SuperAdminUserProtectException()
        }

        currentUser.email = userProfileDto.email
        currentUser.firstName = userProfileDto.firstName
        currentUser.lastName = userProfileDto.lastName

        if (isEmailUpdate) {
            currentUser.status = UserStatusEnum.VERIFYING.status // TODO change status for email verification
            val userVerification = createUserVerification(currentUser)
            sendUserVerificationEmail(currentUser, userVerification.verificationCode!!)

        }

        userRepository?.save(currentUser)

    }

    @Transactional
    override fun updateUserPassword(updatePasswordDto: UserUpdatePasswordDto) {

        val currentUser = userService?.currentUser()
        currentUser!!.password = passwordEncoder!!.encode(updatePasswordDto.password)

        if (userService?.adminUserRestrictionCheck()!!) {
            throw SuperAdminUserProtectException()
        }

        sendUserUpdatePasswordNotificationEmail(currentUser)

        userRepository?.save(currentUser)

    }

    @Transactional
    override fun resendUserVerification() {
        // get current user
        val currentUser = userService?.currentUser()
        // check user status
        if (!checkUserStatus(currentUser!!, UserStatusEnum.VERIFYING)) {
            throw SystemErrorException("User has been verified.")
        }
        // revoke latest verification code
        userVerificationService?.disableExpiredUserVerification(currentUser)
        // create new user verification
        val userVerification = createUserVerification(currentUser)
        sendUserVerificationEmail(currentUser, userVerification.verificationCode!!)

    }

    fun createUserVerification(currentUser: User) : UserVerification {
        return userVerificationService?.createUserVerification(currentUser)!!
    }

    fun sendUserVerificationEmail(currentUser: User, verificationCode: String) {

        val verificationUrl = "${verificationUrlPrefix}/${currentUser.id}/${verificationCode}"

        val content =
                "Hi! ${currentUser.firstName + ' ' + currentUser.lastName},  <br>" +
                        "<br>"+
                        "Please click the link below for user verification <br>" +
                        "<a href=\"${verificationUrl}\">${verificationUrl}</a><br>" +
                        "<br>"+
                        "<br>"+
                        "Best Regards, <br>" +
                        "The Millennium Method<br>"

        mailService?.sendEmailMessage(currentUser.email!!, "$emailHeaderPrefix User Verification", content)

    }

    fun sendUserUpdatePasswordNotificationEmail(currentUser: User) {

        val content =
                "Hi! ${currentUser.firstName + ' ' + currentUser.lastName},  <br>" +
                        "<br>"+
                        "You have updated your account password. If not, Please contact us immediately : millennium.method.ops@gmail.com <br>" +
                        "<br>"+
                        "<br>"+
                        "Best Regards, <br>" +
                        "The Millennium Method<br>"

        mailService?.sendEmailMessage(currentUser.email!!, "$emailHeaderPrefix Password Updated Notification", content)

    }

    fun convertUserProfileDto(user: User) : UserProfileDto {

        var userProfileDto = UserProfileDto()

        userProfileDto.firstName = user.firstName
        userProfileDto.lastName = user.lastName
        userProfileDto.email = user.email
        userProfileDto.confirmEmail = user.email

        return userProfileDto

    }

    fun checkUserStatus(currentUser: User, targetUserStatus: UserStatusEnum): Boolean {
        return currentUser.status == targetUserStatus.status
    }

}
