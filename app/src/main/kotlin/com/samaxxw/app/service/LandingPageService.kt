package com.samaxxw.app.service

import com.samaxxw.app.dto.LandingPageUserProductResponse
import com.samaxxw.model.Product
import java.util.*

interface LandingPageService {

    fun findTop10ByStatusOrderByCreateDatetimeDesc(status: Int) : MutableList<Product>?

    fun getLandingPageProduct() : TreeMap<Int, LandingPageUserProductResponse>

}
