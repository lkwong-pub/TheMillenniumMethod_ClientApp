package com.samaxxw.app.service

import com.samaxxw.app.dto.UserRegistrationDto
import com.samaxxw.app.dto.form.user.UserProfileDto
import com.samaxxw.app.dto.form.user.UserUpdatePasswordDto
import com.samaxxw.model.Role
import com.samaxxw.model.User
import org.springframework.transaction.annotation.Transactional

interface UserAccountService {

    fun registration(registration: UserRegistrationDto): User

    fun getUserProfileDto(): UserProfileDto

    fun updateUserProfile(userProfileDto: UserProfileDto, isEmailUpdate: Boolean)

    fun updateUserPassword(updatePasswordDto: UserUpdatePasswordDto)

    fun resendUserVerification()

}