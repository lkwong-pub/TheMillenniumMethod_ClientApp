package com.samaxxw.app.service

import com.samaxxw.model.Role
import com.samaxxw.model.Sales
import com.samaxxw.model.User
import com.samaxxw.model.UserVerification
import javax.servlet.http.HttpServletRequest

interface UserVerificationService {

    fun checkVerificationCode(userId: Long, verificationCode: String)

    fun createUserVerification(currentUser: User) : UserVerification

    fun disableExpiredUserVerification(currentUser: User)

}