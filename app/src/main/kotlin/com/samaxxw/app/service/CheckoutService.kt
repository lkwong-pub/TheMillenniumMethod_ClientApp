package com.samaxxw.app.service

import com.samaxxw.app.dto.CheckoutItemDto
import com.samaxxw.model.Role
import javax.servlet.http.HttpServletRequest

interface CheckoutService {

    fun validateCheckoutItem(productId: Long) : CheckoutItemDto

    fun checkoutProcess() : Map<String, Any>

    fun completeCheckout()

}