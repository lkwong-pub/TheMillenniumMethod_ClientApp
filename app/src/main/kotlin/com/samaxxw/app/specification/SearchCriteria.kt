package com.samaxxw.app.specification

class SearchCriteria {
    val key: String? = null
    val operation: String? = null
    val value: Any? = null
}