package com.samaxxw.app.exception

class ProductResourcesAccessDeniedException() : RuntimeException() {
    constructor(message: String) : this()

}