package com.samaxxw.app.controller

import com.samaxxw.app.controller.UserController.Companion.ROOT
import com.samaxxw.app.dto.form.user.UserProfileDto
import com.samaxxw.app.dto.form.user.UserRequestResetPasswordForm
import com.samaxxw.app.dto.form.user.UserResetPasswordForm
import com.samaxxw.app.dto.form.user.UserUpdatePasswordDto
import com.samaxxw.app.service.UserAccountService
import com.samaxxw.app.service.UserVerificationService
import com.samaxxw.app.service.ui.UserUIService
import com.samaxxw.controller.AdvancedBaseRestController
import com.samaxxw.controller.searchModel.SearchModelName.Companion.SEARCH_FILTER_NAME_USER
import com.samaxxw.dto.create.CreateUserDto
import com.samaxxw.dto.form.common.MessageBox
import com.samaxxw.dto.form.common.MessageBoxTypeEnum
import com.samaxxw.dto.form.user.UserEditFormDto
import com.samaxxw.dto.model.RoleDto
import com.samaxxw.dto.model.UserDto
import com.samaxxw.dto.paged.PagedDto
import com.samaxxw.dto.update.UpdateUserDto
import com.samaxxw.dtoservice.BaseDtoService
import com.samaxxw.dtoservice.RoleDtoService
import com.samaxxw.dtoservice.UserDtoService
import com.samaxxw.exception.ResourceNotFoundException
import com.samaxxw.exception.SystemErrorException
import com.samaxxw.exception.ValidationException
import com.samaxxw.service.UserResetPasswordService
import com.samaxxw.service.UserService
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.ui.ModelMap
import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.*
import org.springframework.web.servlet.mvc.support.RedirectAttributes
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import javax.validation.Valid

@Controller
@RequestMapping(value = [ROOT])
class UserController : ClientAdvancedBaseRestController<UserDto, CreateUserDto, UpdateUserDto>() {

    @Autowired
    var userDtoService : UserDtoService? = null

    @Autowired
    var userService : UserService? = null

    @Autowired
    var roleDtoService : RoleDtoService? = null

    @Autowired
    val userAccountService : UserAccountService? = null

    @Autowired
    val userVerificationService : UserVerificationService? = null

    @Autowired
    val userResetPasswordService : UserResetPasswordService? = null

    @Autowired
    val userUIService: UserUIService? = null

    override val dtoService: BaseDtoService<UserDto>
        get() = userDtoService!!


    companion object {
        val log = LoggerFactory.getLogger(this.javaClass)!!
        const val ROOT = "/user"
        const val BINDRESULT_USERPROFILE_KEY = "org.springframework.validation.BindingResult.userProfile"
        const val BINDRESULT_USERUPDATE_PASSWORD_KEY = "org.springframework.validation.BindingResult.userUpdatePassword"
        const val BINDRESULT_USERREQUEST_RESET_PASSWORD_KEY = "org.springframework.validation.BindingResult.userRequestResetPasswordForm"
        const val BINDRESULT_USER_RESET_PASSWORD_KEY = "org.springframework.validation.BindingResult.userResetPasswordForm"


    }

    @ModelAttribute("userProfile")
    fun userProfileDto(): UserProfileDto {
        return UserProfileDto()
    }

    @ModelAttribute("userUpdatePassword")
    fun userUpdatePassword(): UserUpdatePasswordDto {
        return UserUpdatePasswordDto()
    }

    @ModelAttribute("userResetPasswordForm")
    fun userResetPasswordForm(): UserResetPasswordForm {
        return UserResetPasswordForm()
    }

    @ModelAttribute("userRequestResetPasswordForm")
    fun userRequestResetPasswordForm(): UserRequestResetPasswordForm {
        return UserRequestResetPasswordForm()
    }

    @ModelAttribute
    fun moduleVal(model: ModelMap, request: HttpServletRequest) {

        model.addAttribute("searchFilter", SEARCH_FILTER_NAME_USER)
//        model.addAttribute("url", baseUrl + "/" + ROOT_URL + "/")
//        model.addAttribute("listUrl", baseUrl + "/" + ROOT_URL + "/")
        model.addAttribute("addUrl", "$ROOT/add")
        model.addAttribute("editUrl", "$ROOT/edit")
        model.addAttribute("userTrxUrl", "/trx/user")
        model.addAttribute("userPurchaseUrl", "/purchase/user")
        model.addAttribute("userGrantUrl", "/grant/user")


//        model.addAttribute("changeStatusUrl", baseUrl + "/" + ROOT_URL + "/status/")
        // Role List
        val roleList : MutableList<RoleDto> = roleDtoService!!.findAll()
        model.addAttribute("roleList", roleList)

    }

    override fun searchWithPaging(
            @RequestParam(value = "page", required = true, defaultValue = DEFAULT_PAGE_NUM) page: Int?,
            @RequestParam(value = "per_page", required = true, defaultValue = DEFAULT_PAGE_SIZE) per_page: Int?,
            @RequestParam(value = "sort", defaultValue = DEFAULT_SORT, required = false) sortField: String,
            @RequestParam(value = "direction", defaultValue = DEFAULT_SORT_DIR, required = false) direction: String,
            request: HttpServletRequest, response: HttpServletResponse): PagedDto<UserDto> {
        return super.searchWithPaging(page, per_page, sortField,  direction, request, response)
    }

    fun userRoleHandler(userDto: UserDto) : LinkedHashMap<String, Boolean> {

        val userRoleMap : LinkedHashMap<String, Boolean> = LinkedHashMap()
        val roleList = roleDtoService!!.findAll()
        roleList.forEach { role ->

            var selected = false
            userDto.roles!!.forEach{
                userRole ->
                if (userRole.name == role.name) {
                    selected = true
                }
            }

            userRoleMap[role.name!!] = selected

        }

        return userRoleMap

    }


    /**
     * user profile page
     */
    @GetMapping(value = ["/profile"])
    fun userProfilePage(model: Model) : String {

        userUIService!!.refreshProfileModel(model)
//        model.addAttribute(PAGE_HEADER, "User Profile")
//        model.addAttribute("userProfile", userAccountService?.getUserProfileDto())

        if (model.asMap().containsKey(BIND_RESULT_KEY)) {
            model.addAttribute(BINDRESULT_USERPROFILE_KEY,
                    model.asMap()[BIND_RESULT_KEY])
        }

        model.addAttribute(AdvancedBaseRestController.BREABCRUMB, breadCrumbService?.getUserProfileRootBreadCrumb())


        return getViewPath("user/profile", request!!)
    }

    /**
     * submit user profile update
     */
    @PostMapping(value = ["/profile/submit"])
    fun updateUserProfile(@ModelAttribute("userProfile") @Valid userProfileDto: UserProfileDto,
                          result: BindingResult,
                          attr : RedirectAttributes,
                          model: Model): String {

        userUIService!!.userProfileModel(model)
        var isEmailUpdated = false
        val currentUser = userService?.currentUser()
        val existing = userService!!.findByEmail(userProfileDto.email!!)
        if (existing != null && currentUser!!.id != existing.id) {
            result.rejectValue("email", "", "Email registered.")
        } else if (existing == null) {
            // E-mail updated
            isEmailUpdated = true
        }

        if (result.hasErrors()) {
            return getViewPath("user/profile", request!!)
        }

        try {
            userAccountService?.updateUserProfile(userProfileDto, isEmailUpdated)
        } catch (e: Exception) {
            model.addAttribute(SYSTEM_MSG_KEY, MessageBox(MessageBoxTypeEnum.DANGER, e.message))
            e.printStackTrace()
            return getViewPath("user/profile", request!!)
        }

        if (isEmailUpdated) {
            // Force use to logout the system
            return "redirect:/logout/updateProfile"
        }

//        attr.addAttribute(AdvancedBaseRestController.SYSTEM_MSG_KEY, AdvancedBaseRestController.SYSTEM_MSG_KEY).addFlashAttribute(AdvancedBaseRestController.SYSTEM_MSG_KEY, MessageBox(MessageBoxTypeEnum.SUCCESS, "Profile Updated"))
        model.addAttribute(SYSTEM_MSG_KEY, MessageBox(MessageBoxTypeEnum.SUCCESS, "Profile Updated"))
        userUIService!!.refreshProfileModel(model)

        return getViewPath("user/profile", request!!)
    }

    /**
     * update user password page
     */
    @GetMapping(value = ["/updatePassword"])
    fun updatePasswordPage(model: Model) : String {

        userUIService!!.updatePasswordModel(model)

        if (model.asMap().containsKey(BIND_RESULT_KEY)) {
            model.addAttribute(BINDRESULT_USERUPDATE_PASSWORD_KEY,
                    model.asMap()[BIND_RESULT_KEY])
        }

        model.addAttribute(AdvancedBaseRestController.BREABCRUMB, breadCrumbService?.getUpdatePasswordRootBreadCrumb())

        return getViewPath("user/updatePassword", request!!)
    }


    /**
     * submit user password update
     */
    @PostMapping(value = ["/updatePassword/submit"])
    fun updateUserPassword(@ModelAttribute("userUpdatePassword") @Valid userUpdatePasswordDto: UserUpdatePasswordDto,
                          result: BindingResult,
                           model: Model,
                          attr : RedirectAttributes): String {

        userUIService!!.updatePasswordModel(model)
        if (result.hasErrors()) {
            return getViewPath("user/updatePassword", request!!)
        }

        try {
            userAccountService?.updateUserPassword(userUpdatePasswordDto)
        } catch (e: Exception) {
//            attr.addAttribute(AdvancedBaseRestController.SYSTEM_MSG_KEY, AdvancedBaseRestController.SYSTEM_MSG_KEY).addFlashAttribute(AdvancedBaseRestController.SYSTEM_MSG_KEY, MessageBox(MessageBoxTypeEnum.DANGER, e.message))
            model.addAttribute(SYSTEM_MSG_KEY, MessageBox(MessageBoxTypeEnum.DANGER, e.message))
            e.printStackTrace()
            return getViewPath("user/updatePassword", request!!)

        }
        model.addAttribute(SYSTEM_MSG_KEY, MessageBox(MessageBoxTypeEnum.SUCCESS, "Password Updated"))

//        attr.addAttribute(AdvancedBaseRestController.SYSTEM_MSG_KEY, AdvancedBaseRestController.SYSTEM_MSG_KEY).addFlashAttribute(AdvancedBaseRestController.SYSTEM_MSG_KEY, MessageBox(MessageBoxTypeEnum.SUCCESS, "Password Updated"))
        return getViewPath("user/updatePassword", request!!)

    }


    /**
     * User verification
     */
    @GetMapping(value = ["/verification/{userId}/{verificationCode}"])
    fun userVerification(
            @PathVariable("userId") userId: Int,
            @PathVariable("verificationCode") verificationCode: String,
            model: Model) : String {

        try {
            userVerificationService?.checkVerificationCode(userId.toLong(), verificationCode)
        } catch (t: Throwable) {

            model.addAttribute(AdvancedBaseRestController.SYSTEM_MSG_KEY, MessageBox(MessageBoxTypeEnum.DANGER, "User verification failed - ${t.message}"))
            return "base/publicMessage"
        }

        model.addAttribute(AdvancedBaseRestController.SYSTEM_MSG_KEY, MessageBox(MessageBoxTypeEnum.SUCCESS, "User verification success."))
        return "base/publicMessage"
    }

    /**
     * User request reset password page
     */
    @GetMapping(value = ["/resetPassword"])
    fun requestResetPasswordPage(
            model: Model) : String {

        if (model.asMap().containsKey(BIND_RESULT_KEY)) {
            model.addAttribute(BINDRESULT_USERREQUEST_RESET_PASSWORD_KEY, model.asMap()[BIND_RESULT_KEY])
        }

        return "forget_password"
    }

    /**
     * User request reset password submission
     */
    @PostMapping(value = ["/resetPassword"])
    fun submitRequestResetPasswordPage(
            @ModelAttribute("userRequestResetPasswordForm") @Valid userRequestResetPasswordForm: UserRequestResetPasswordForm,
            result: BindingResult,
            model: Model,
            attr : RedirectAttributes) : String {

        if (result.hasErrors()) {
            attr.addAttribute(BIND_RESULT_KEY, BIND_RESULT_KEY).addFlashAttribute(BIND_RESULT_KEY, result)
            return "redirect:/user/resetPassword"
        } else {

            try {
                userResetPasswordService?.createResetPasswordRequest(userRequestResetPasswordForm.email!!)
            } catch (e: Exception) {
                model.addAttribute(AdvancedBaseRestController.SYSTEM_MSG_KEY, MessageBox(MessageBoxTypeEnum.DANGER, e.message))
                return "forget_password"
            }
            return "redirect:/user/resetPassword?success"
        }
    }

    /**
     * User reset password page
     */
    @GetMapping(value = ["/resetPassword/{userId}/{requestId}"])
    fun resetPasswordPage(
            @PathVariable("userId") userId: Int,
            @PathVariable("requestId") requestId: String,
            model: Model) : String {

        try {
            userResetPasswordService?.checkResetPasswordRequest(userId.toLong(), requestId)
            model.addAttribute("userResetPasswordForm", convertoUserResetPasswordForm(userId.toLong(), requestId))
            if (model.asMap().containsKey(BIND_RESULT_KEY)) {
                model.addAttribute(BINDRESULT_USER_RESET_PASSWORD_KEY, model.asMap()[BIND_RESULT_KEY])
            }
        } catch (t: Throwable) {
            model.addAttribute(AdvancedBaseRestController.SYSTEM_MSG_KEY, MessageBox(MessageBoxTypeEnum.DANGER, "${t.message}"))
            return "base/publicMessage"
        }
        // reset password page
        return "reset_password"
    }

    /**
     * User reset password submission
     */
    @PostMapping(value = ["/resetPassword/submit"])
    fun submitResetPasswordPage(
            @ModelAttribute("userResetPasswordForm") @Valid userResetPasswordForm: UserResetPasswordForm,
            result: BindingResult,
            model: Model,
            attr : RedirectAttributes) : String {

        if (result.hasErrors()) {
            attr.addAttribute(BIND_RESULT_KEY, BIND_RESULT_KEY).addFlashAttribute(BIND_RESULT_KEY, result)
            return "redirect:/user/resetPassword/${userResetPasswordForm.userId}/${userResetPasswordForm.requestId}"
        } else {
            try {
                submitResetPasswordFormValidation(userResetPasswordForm)
            } catch (ve : ValidationException) {
                result.rejectValue(ve.field, ve.errorCode!!, ve.message!!)
                attr.addAttribute(BIND_RESULT_KEY, BIND_RESULT_KEY).addFlashAttribute(BIND_RESULT_KEY, result)
                return "redirect:/user/resetPassword/${userResetPasswordForm.userId}/${userResetPasswordForm.requestId}"
            }
            userResetPasswordService?.resetPassword(userResetPasswordForm.userId!!, userResetPasswordForm.requestId!!, userResetPasswordForm.password!!)
            model.addAttribute(AdvancedBaseRestController.SYSTEM_MSG_KEY, MessageBox(MessageBoxTypeEnum.SUCCESS, "Password Reset successfully!"))
            return "base/publicMessage"
        }
    }

    /**
     * Resend user verification email
     */
    @GetMapping(value = ["/verification/resend"], consumes = ["application/json"])
    fun resendVerification(): ResponseEntity<MessageBox> {
        try {
            userAccountService?.resendUserVerification()
            return ResponseEntity(MessageBox(MessageBoxTypeEnum.SUCCESS, "Verification e-mail sent!"), HttpStatus.OK)
        } catch (e: Exception) {
            return ResponseEntity(MessageBox(MessageBoxTypeEnum.DANGER, e.message), HttpStatus.BAD_REQUEST)
        }
    }

    fun convertoUserResetPasswordForm(userId: Long, requestId: String) : UserResetPasswordForm {
        // TODO move to mapper
        val result = UserResetPasswordForm()
        result.userId = userId
        result.requestId = requestId
        return result
    }

    fun submitResetPasswordFormValidation(userResetPasswordForm: UserResetPasswordForm) {
        if (userResetPasswordForm.password != userResetPasswordForm.confirmPassword) {
            throw ValidationException("password", "", "The password fields must match be matched")
        }
        return
    }
}