package com.samaxxw.app.controller

import com.samaxxw.app.config.CaptchaSettings
import com.samaxxw.app.dto.UserRegistrationDto
import com.samaxxw.app.service.UserAccountService
import com.samaxxw.app.service.impl.CaptchaValidator
import com.samaxxw.controller.AbstractRestHandler
import com.samaxxw.service.UserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.transaction.annotation.Transactional
import org.springframework.ui.Model
import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.ModelAttribute
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import javax.validation.Valid

@Controller
@RequestMapping("/registration")
class UserRegistrationController : AbstractRestHandler() {

    @Autowired
    val userService: UserService? = null

    @Autowired
    val captchaSettings: CaptchaSettings? = null

    @Autowired
    private val userAccountService: UserAccountService? = null

    @Autowired
    val captchaValidator: CaptchaValidator? = null

    @ModelAttribute("user")
    fun userRegistrationDto(): UserRegistrationDto {
        return UserRegistrationDto()
    }

    @GetMapping
    fun showRegistrationForm(model: Model): String {
        model.addAttribute("siteKey", captchaSettings!!.site)
        return "registration"
    }

    @PostMapping
    @Transactional
    fun registerUserAccount(@ModelAttribute("user") @Valid userDto: UserRegistrationDto,
                            result: BindingResult): String {

        // reCaptcha validation
        if (!captchaValidator?.validateCaptcha(userDto.token!!)!!) {
            result.rejectValue("token", "", "You are BOT")
        }

        val existing = userService!!.findByEmail(userDto.email!!)
        if (existing != null) {
            result.rejectValue("email", "", "There is already an account registered with that email")
        }

        if (result.hasErrors()) {
            return "registration"
        }

        userAccountService!!.registration(userDto)
        return "redirect:/registration?success"
    }

}
