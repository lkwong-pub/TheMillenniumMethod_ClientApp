package com.samaxxw.app.controller

import com.samaxxw.app.config.CaptchaSettings
import com.samaxxw.app.service.LandingPageService
import com.samaxxw.controller.AbstractRestHandler
import com.samaxxw.controller.AdvancedBaseRestController
import com.samaxxw.controller.template.NavKeyConstant
import com.samaxxw.dto.form.common.MessageBox
import com.samaxxw.dto.form.common.MessageBoxTypeEnum
import com.samaxxw.mongo.service.PageResourcesService
import com.samaxxw.util.ProductStatusEnum
import org.apache.tomcat.util.http.fileupload.IOUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.core.io.InputStreamResource
import org.springframework.core.io.UrlResource
import org.springframework.core.io.support.ResourceRegion
import org.springframework.http.*
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.*
import org.springframework.web.servlet.mvc.support.RedirectAttributes
import java.io.IOException
import java.lang.Long.min
import java.net.URL
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Controller
class MainController : AbstractRestHandler() {

    @Autowired
    val landingPageService: LandingPageService? = null

    @Autowired
    val pageResourcesService: PageResourcesService? = null

    @Autowired
    val captchaSettings: CaptchaSettings? = null

    @Value("\${app.resources-server}")
    val resourceServerUrl : String? = null

    /**
     * Landing Page product List
     */
    @GetMapping("/")
    fun getLandingPageProductList(
            model: Model
    ): String {

        model.addAttribute("productList", landingPageService?.getLandingPageProduct())
        model.addAttribute(AdvancedBaseRestController.NAV_KEY, NavKeyConstant.NAV_HOME)

        return getViewPath("index", request!!)
    }

//
//    @GetMapping("/index")
//    fun appIndex(): String {
//        return "app/themethod/index"
//    }

    /**
     * Path should same as Product Resources Url setting - for validation
     */
    @RequestMapping(value = ["/themethod/{language}/"], method = [RequestMethod.GET])
    fun methodIndex(
            @PathVariable("language") language: String,
            model: Model): String {
        model.addAttribute("language", language)
        model.addAttribute("pageResources", pageResourcesService?.getPageResourcesMapBySection("index", language))
        return "app/themethod/index"
    }


    @RequestMapping(value = ["/themethod/{language}/{section}.html"], method = [RequestMethod.GET])
    fun methodSection(@PathVariable("section") section: String,
                      @PathVariable("language") language: String,
                      model: Model): String {
        model.addAttribute("language", language)
        model.addAttribute("pageResources", pageResourcesService?.getPageResourcesMapBySection(section, language))

        return "app/themethod/$section"
    }

//    @GetMapping("/")
//    fun root(): String {
//        return "index"
//    }

    @GetMapping("/login")
    fun login(model: Model, response: HttpServletResponse): String {
        model.addAttribute("siteKey", captchaSettings!!.site)
        response.addHeader("PageType", "LOGIN")
        response.addHeader("Redirect", "/login")
        return "login"
    }

    @GetMapping("/user")
    fun userIndex(): String {
        return "user/index"
    }

    @GetMapping("/accessDenied")
    fun accessDenied(response: HttpServletResponse): String {
        response.addHeader("PageType", "ACCESS_DENIED")
        response.addHeader("Redirect", "/accessDenied")
        return "401"
    }

    @RequestMapping(value = ["/themethod/{language}/resources/{type}/{fileName}"], method = [RequestMethod.GET])
    @Throws(IOException::class)
    fun getImageAsByteArray(
            @PathVariable("language") language: String,
            @PathVariable("type") type : String,
            @PathVariable("fileName") fileName : String,
            response: HttpServletResponse) {

        val target = "$resourceServerUrl/themethod/$type/$fileName"
//        val target = "https://www.w3schools.com/html/mov_bbb.mp4"
//        val target = "$resourceServerUrl/$type/test.png"

        val input1 = URL(target).openConnection()
        input1.contentType
        val input = URL(target).openStream()

//        response.contentType = MediaType.IMAGE_JPEG_VALUE
        response.contentType = input1.contentType

        IOUtils.copy(input, response.outputStream)
    }

//    @RequestMapping(value = ["/themethod/{language}/resources/{type}/{section}/{fileName}"], method = [RequestMethod.GET])
//    @Throws(IOException::class)
//    fun getVideo(
//            @PathVariable("language") language: String,
//            @PathVariable("type") type : String,
//            @PathVariable("section") section : String,
//            @PathVariable("fileName") fileName : String,
//            response: HttpServletResponse) : ResponseEntity<InputStreamResource> {
//
//        // TODO code clean
//
//        val target = "$resourceServerUrl/themethod/$type/$section/$fileName"
////        val target = "https://www.w3schools.com/html/mov_bbb.mp4" // Dummy video
////        val target = "$resourceServerUrl/video/test.mp4"
//
//
//        val input1 = URL(target).openConnection()
//        val input = URL(target).openStream()
//
//        val inputStreamResource = InputStreamResource(input)
//
//        var httpHeaders = HttpHeaders()
//        httpHeaders.contentLength = input1.contentLengthLong
//        httpHeaders.contentType = MediaType.valueOf(input1.contentType)
//        val httpRange = HttpRange.createByteRange(0, input1.contentLengthLong)
//        httpHeaders.range = mutableListOf(httpRange)
//        return ResponseEntity(inputStreamResource, httpHeaders, HttpStatus.OK)
//
//
//    }

    @GetMapping("/themethod/{language}/resources/{type}/{section}/{fileName}")
    fun getVideo(
            @PathVariable("language") language: String,
            @PathVariable("type") type : String,
            @PathVariable("section") section : String,
            @PathVariable("fileName") fileName : String,
            @RequestHeader headers: HttpHeaders,
            response: HttpServletResponse): ResponseEntity<ResourceRegion> {

        val target = "$resourceServerUrl/themethod/$type/$section/$fileName"

        val video = UrlResource(target)
        val region = resourceRegion(video, headers)
        return ResponseEntity.status(HttpStatus.PARTIAL_CONTENT)
                .contentType(MediaTypeFactory
                        .getMediaType(video)
                        .orElse(MediaType.APPLICATION_OCTET_STREAM))
                .body(region)

    }

    private fun resourceRegion(video: UrlResource, headers: HttpHeaders): ResourceRegion {
        val contentLength = video.contentLength()
        val range = headers.range.firstOrNull()
        return if (range != null) {
            val start = range.getRangeStart(contentLength)
            val end = range.getRangeEnd(contentLength)
            val rangeLength = min(1 * 1024 * 1024, end - start + 1)
            ResourceRegion(video, start, rangeLength)
        } else {
            val rangeLength = min(1 * 1024 * 1024, contentLength)
            ResourceRegion(video, 0, rangeLength)
        }
    }

    @RequestMapping(value = ["/logout/updateProfile"])
    fun logout(request : HttpServletRequest, model : Model) : String {
        SecurityContextHolder.clearContext()
        val session = request.getSession(false)
        if (session != null) {
            session.invalidate()
        }

        model.addAttribute(AdvancedBaseRestController.SYSTEM_MSG_KEY, MessageBox(MessageBoxTypeEnum.INFO, "User Profile Updated. Please login again."))
        return "login"

//        attr.addAttribute(AdvancedBaseRestController.SYSTEM_MSG_KEY, AdvancedBaseRestController.SYSTEM_MSG_KEY).addFlashAttribute(AdvancedBaseRestController.SYSTEM_MSG_KEY, MessageBox(MessageBoxTypeEnum.WARNING, "Attachment not found"))
//
//        return "redirect:/login"
    }
}
