package com.samaxxw.app.controller

import com.samaxxw.app.service.ui.BreadCrumbService
import com.samaxxw.controller.AbstractRestHandler
import org.springframework.beans.factory.annotation.Autowired

abstract class ClientAbstractRestHandler : AbstractRestHandler() {

    @Autowired
    var breadCrumbService : BreadCrumbService? = null

}