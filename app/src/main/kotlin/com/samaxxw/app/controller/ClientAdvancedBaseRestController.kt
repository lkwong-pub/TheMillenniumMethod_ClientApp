package com.samaxxw.app.controller

import com.samaxxw.app.service.ui.BreadCrumbService
import com.samaxxw.controller.AdvancedBaseRestController
import com.samaxxw.dto.update.UpdateDto
import com.samaxxw.dtoservice.AdvancedBaseDtoService
import com.samaxxw.dtoservice.UserDtoService
import com.samaxxw.exception.DataFormatException
import com.samaxxw.model.User
import com.samaxxw.service.UserService
import org.apache.commons.lang3.ObjectUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.*
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse


abstract class ClientAdvancedBaseRestController<TO, CreateTO, UpdateTO : UpdateDto> : AdvancedBaseRestController<TO, CreateTO, UpdateTO>() {

    @Autowired
    var breadCrumbService : BreadCrumbService? = null


}