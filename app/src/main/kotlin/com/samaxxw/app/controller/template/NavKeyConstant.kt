package com.samaxxw.controller.template


class NavKeyConstant {

    companion object {

        val NAV_HOME = "navHome"
        val NAV_USER_PROFILE = "navUserProfile"
        val NAV_USER_UPDATE_PASSWORD = "navUpdatePassword"
        val NAV_TRX = "navTrx"
        val NAV_PRODUCT = "navProduct"
        val NAV_PURCHASED_RECORD = "navPurchasedRecord"

    }
}