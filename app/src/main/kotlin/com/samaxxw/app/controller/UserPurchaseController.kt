package com.samaxxw.app.controller

import com.samaxxw.app.controller.UserPurchaseController.Companion.ROOT
import com.samaxxw.app.controller.searchModel.UserPurchaseHistorySearchModel
import com.samaxxw.controller.AdvancedBaseRestController
import com.samaxxw.controller.searchModel.SearchModelName.Companion.SEARCH_FILTER_NAME_SALES_TRX
import com.samaxxw.dto.create.CreateSalesTrxDto
import com.samaxxw.dto.create.UpdateSalesTrxDto
import com.samaxxw.dto.model.ProductDto
import com.samaxxw.dto.model.SalesTrxDto
import com.samaxxw.dtoservice.BaseDtoService
import com.samaxxw.dtoservice.ProductDtoService
import com.samaxxw.dtoservice.SalesTrxDtoService
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.ui.ModelMap
import org.springframework.web.bind.annotation.ModelAttribute
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RequestParam
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse


@Controller
@RequestMapping(value = [ROOT])
class UserPurchaseController : ClientAdvancedBaseRestController<SalesTrxDto, CreateSalesTrxDto, UpdateSalesTrxDto>() {

    @Autowired
    @Qualifier("UserPurchaseHistoryService")
    var salesTrxDtoService : SalesTrxDtoService? = null

    @Autowired
    @Qualifier("productClientDtoService")
    val productDtoService : ProductDtoService? = null


    override val dtoService: BaseDtoService<SalesTrxDto>
        get() = salesTrxDtoService!!


    companion object {
        val log = LoggerFactory.getLogger(this.javaClass)!!
        const val ROOT = "/purchase"
//        const val BINDRESULT_USEREDITFORM_KEY = "org.springframework.validation.BindingResult.userEditForm"
        
    }

    @ModelAttribute
    fun moduleVal(model: ModelMap, request: HttpServletRequest) {

        model.addAttribute("searchFilter", SEARCH_FILTER_NAME_SALES_TRX)
//        model.addAttribute("url", baseUrl + "/" + ROOT_URL + "/")
//        model.addAttribute("listUrl", baseUrl + "/" + ROOT_URL + "/")
        model.addAttribute("addUrl", "$ROOT/add")
        model.addAttribute("editUrl", "$ROOT/edit")
//        model.addAttribute("changeStatusUrl", baseUrl + "/" + ROOT_URL + "/status/")
        // Role List
        val productList : MutableList<ProductDto> = productDtoService!!.findAll()
//        model.addAttribute("productList", productList)

//        model.addAttribute(AdvancedBaseRestController.NAV_KEY, NavKeyConstant.NAV_PURCHASED_RECORD)


    }

    @RequestMapping(value = ["/user/list"], method = [RequestMethod.GET, RequestMethod.POST])
    fun list(
            @RequestParam(value = "page", required = true, defaultValue = DEFAULT_PAGE_NUM) page: Int?,
            @RequestParam(value = "per_page", required = true, defaultValue = DEFAULT_PAGE_SIZE) per_page: Int?,
            @RequestParam(value = "sort", defaultValue = DEFAULT_SORT, required = false) sortField: String,
            @RequestParam(value = "direction", defaultValue = DEFAULT_SORT_DIR, required = false) direction: String,
            userOwnedProductSearchModel: UserPurchaseHistorySearchModel,
            model: Model,
            response: HttpServletResponse,
            request: HttpServletRequest
    ): String {

        val userId = authenticationUserService?.currentUser()!!.id!!.toLong()
        if (request.method == RequestMethod.POST.name) {
            userOwnedProductSearchModel.userId = userId
            request.session.setAttribute(SEARCH_FILTER_NAME_SALES_TRX, userOwnedProductSearchModel)
        } else if (request.method == RequestMethod.GET.name) {
            val userOwnedProductSearchFilter = UserPurchaseHistorySearchModel()
            userOwnedProductSearchFilter.userId = userId // default filter by user ID
            request.session.setAttribute(SEARCH_FILTER_NAME_SALES_TRX, userOwnedProductSearchFilter)
        }

        val result = super.searchWithPaging(page, per_page, sortField, direction, request, response)
        model.addAttribute(PAGE_HEADER, "User Purchase Record")
        model.addAttribute("list", result)
        model.addAttribute("userId", userId)

        model.addAttribute(AdvancedBaseRestController.BREABCRUMB, breadCrumbService?.getPurchasedItemRootBreadCrumb())

        return getViewPath("user/purchase/list", request)
    }

}