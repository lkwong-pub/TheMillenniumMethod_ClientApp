package com.samaxxw.app.controller

import com.samaxxw.app.controller.UserTrxController.Companion.ROOT
import com.samaxxw.app.controller.searchModel.UserPurchaseHistorySearchModel
import com.samaxxw.app.service.UserPurchaseHistoryService
import com.samaxxw.app.service.ui.BreadCrumbService
import com.samaxxw.controller.AbstractRestHandler
import com.samaxxw.controller.AbstractRestHandler.Companion.DEFAULT_PAGE_NUM
import com.samaxxw.controller.AbstractRestHandler.Companion.DEFAULT_PAGE_SIZE
import com.samaxxw.controller.AbstractRestHandler.Companion.DEFAULT_SORT
import com.samaxxw.controller.AbstractRestHandler.Companion.DEFAULT_SORT_DIR
import com.samaxxw.controller.AdvancedBaseRestController
import com.samaxxw.controller.AdvancedBaseRestController.Companion.PAGE_HEADER
import com.samaxxw.controller.searchModel.SearchModelName.Companion.SEARCH_FILTER_NAME_SALES_TRX
import com.samaxxw.controller.searchModel.SearchModelName.Companion.SEARCH_FILTER_NAME_USER
import com.samaxxw.controller.template.NavKeyConstant
import com.samaxxw.dto.create.CreateSalesTrxDto
import com.samaxxw.dto.create.UpdateSalesTrxDto
import com.samaxxw.dto.model.ProductDto
import com.samaxxw.dto.model.SalesTrxDto
import com.samaxxw.dtoservice.BaseDtoService
import com.samaxxw.dtoservice.ProductDtoService
import com.samaxxw.dtoservice.SalesTrxDtoService
import com.samaxxw.service.UserService
import com.samaxxw.util.SalesStatus
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.ui.ModelMap
import org.springframework.web.bind.annotation.ModelAttribute
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RequestParam
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse


@Controller
@RequestMapping(value = [ROOT])
class UserTrxController : ClientAbstractRestHandler() {

    @Autowired
    @Qualifier("productClientDtoService")
    val productDtoService : ProductDtoService? = null

    @Autowired
    @Qualifier("UserPurchaseHistoryService")
    val userPurchaseHistoryService : UserPurchaseHistoryService? = null

    @Autowired
    var userService : UserService? = null

    companion object {
        val log = LoggerFactory.getLogger(this.javaClass)!!
        const val ROOT = "/trx"
//        const val BINDRESULT_USEREDITFORM_KEY = "org.springframework.validation.BindingResult.userEditForm"
        
    }

    @ModelAttribute
    fun moduleVal(model: ModelMap, request: HttpServletRequest) {

        model.addAttribute("searchFilter", SEARCH_FILTER_NAME_USER)
//        model.addAttribute("url", baseUrl + "/" + ROOT_URL + "/")
//        model.addAttribute("listUrl", baseUrl + "/" + ROOT_URL + "/")
        model.addAttribute("addUrl", "$ROOT/add")
        model.addAttribute("editUrl", "$ROOT/edit")
//        model.addAttribute("changeStatusUrl", baseUrl + "/" + ROOT_URL + "/status/")
        // Role List
        val productList : MutableList<ProductDto> = productDtoService!!.findAll()
        model.addAttribute("productList", productList)

        model.addAttribute(AdvancedBaseRestController.NAV_KEY, NavKeyConstant.NAV_TRX)


    }

    @RequestMapping(value = ["/list"], method = [RequestMethod.GET, RequestMethod.POST])
    fun list(
            @RequestParam(value = "page", required = true, defaultValue = DEFAULT_PAGE_NUM) page: Int?,
            @RequestParam(value = "per_page", required = true, defaultValue = DEFAULT_PAGE_SIZE) per_page: Int?,
            @RequestParam(value = "sort", defaultValue = "createDatetime", required = false) sortField: String,
            @RequestParam(value = "direction", defaultValue = DEFAULT_SORT_DIR, required = false) direction: String,
            salesTrxSearchModel: UserPurchaseHistorySearchModel,
            model: Model,
            response: HttpServletResponse,
            request: HttpServletRequest
    ): String {

        if (request.method == RequestMethod.POST.name) {
            salesTrxSearchModel.userId = userService?.currentUser()!!.id
            request.session.setAttribute(SEARCH_FILTER_NAME_SALES_TRX, salesTrxSearchModel)
        } else if (request.method == RequestMethod.GET.name) {
            val salesTrxUserFilter = UserPurchaseHistorySearchModel()
            salesTrxUserFilter.userId = userService?.currentUser()!!.id // default filter by user ID
            request.session.setAttribute(SEARCH_FILTER_NAME_SALES_TRX, salesTrxUserFilter)
        }

        val result = userPurchaseHistoryService?.getPurchaseProductList(page, per_page, sortField, direction, request)
        model.addAttribute(PAGE_HEADER, "User Sales Transaction")
        model.addAttribute("list", result)

        model.addAttribute(AdvancedBaseRestController.BREABCRUMB, breadCrumbService?.getTransactionRecordRootBreadCrumb())

        return getViewPath("user/trx/list", request)
    }



}