package com.samaxxw.app.controller

import com.samaxxw.app.controller.ProductClientController.Companion.ROOT
import com.samaxxw.app.dto.ProductResourcesRequestDto
import com.samaxxw.app.dtoService.ProductClientDtoService
import com.samaxxw.app.exception.ProductResourcesAccessDeniedException
import com.samaxxw.app.service.ProductResourceRequestService
import com.samaxxw.controller.AdvancedBaseRestController
import com.samaxxw.controller.searchModel.ProductSearchModel
import com.samaxxw.controller.searchModel.SearchModelName.Companion.SEARCH_FILTER_NAME_PRODUCT
import com.samaxxw.controller.template.NavKeyConstant
import com.samaxxw.dto.create.CreateProductDto
import com.samaxxw.dto.create.UpdateProductDto
import com.samaxxw.dto.form.common.MessageBox
import com.samaxxw.dto.form.common.MessageBoxTypeEnum
import com.samaxxw.dto.model.ProductCategoryDto
import com.samaxxw.dto.model.ProductDto
import com.samaxxw.dtoservice.BaseDtoService
import com.samaxxw.dtoservice.ProductCategoryDtoService
import com.samaxxw.dtoservice.ProductDtoService
import com.samaxxw.service.ProductResourcesService
import com.samaxxw.service.ProductService
import com.samaxxw.service.UserService
import com.samaxxw.util.ProductStatusEnum
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.core.io.ByteArrayResource
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.ui.ModelMap
import org.springframework.web.bind.annotation.*
import org.springframework.web.servlet.mvc.support.RedirectAttributes
import org.thymeleaf.util.StringUtils
import java.math.BigDecimal
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import javax.validation.Valid


@Controller
@RequestMapping(value = [ROOT])
class ProductClientController : ClientAdvancedBaseRestController<ProductDto, CreateProductDto, UpdateProductDto>() {

    @Autowired
    var productDtoService : ProductClientDtoService? = null

    @Autowired
    var productService : ProductService? = null

    @Autowired
    var productCategoryDtoService : ProductCategoryDtoService? = null

    @Autowired
    var productResourcesRequestService : ProductResourceRequestService? = null

    @Autowired
    var productResourcesService : ProductResourcesService? = null

    @Autowired
    var userService : UserService? = null

    override val dtoService: BaseDtoService<ProductDto>
        get() = productDtoService!!


    companion object {
        val log = LoggerFactory.getLogger(this.javaClass)!!
        const val ROOT = "/product"
        const val BINDRESULT_PRODUCTEDITFORM_KEY = "org.springframework.validation.BindingResult.productEditForm"
        
    }

    @ModelAttribute
    fun moduleVal(model: ModelMap, request: HttpServletRequest) {

        model.addAttribute("searchFilter", SEARCH_FILTER_NAME_PRODUCT)
//        model.addAttribute("addUrl", "$ROOT/add")
        model.addAttribute("checkoutUrl", "/checkout/product")
//        model.addAttribute("changeStatusUrl", baseUrl + "/" + ROOT_URL + "/status/")
        // Product Category List
        val productCategoryList : MutableList<ProductCategoryDto> = productCategoryDtoService!!.findAll()
        model.addAttribute("productCategoryList", productCategoryList)

        model.addAttribute(AdvancedBaseRestController.NAV_KEY, NavKeyConstant.NAV_PRODUCT)


    }

    @RequestMapping(value = ["/list"], method = [RequestMethod.GET, RequestMethod.POST])
    fun list(
            @RequestParam(value = "page", required = true, defaultValue = DEFAULT_PAGE_NUM) page: Int?,
            @RequestParam(value = "per_page", required = true, defaultValue = DEFAULT_PAGE_SIZE) per_page: Int?,
            @RequestParam(value = "sort", defaultValue = DEFAULT_SORT, required = false) sortField: String,
            @RequestParam(value = "direction", defaultValue = DEFAULT_SORT_DIR, required = false) direction: String,
            productSearchModel: ProductSearchModel,
            model: Model,
            response: HttpServletResponse,
            request: HttpServletRequest
    ): String {


        if (request.method == RequestMethod.POST.name) {
            request.session.setAttribute(SEARCH_FILTER_NAME_PRODUCT, productSearchModel)
        }

        val result = super.searchWithPaging(page, per_page, sortField, direction, request, response)
        model.addAttribute(PAGE_HEADER, "Product List")
        model.addAttribute("status", ProductStatusEnum.getDisplayMap())
        model.addAttribute("list", result)

        // return for pagination indicator
        model.addAttribute("pageNum", page)
        model.addAttribute(AdvancedBaseRestController.BREABCRUMB, breadCrumbService?.getProductRootBreadCrumb(true))


        return getViewPath("product/list", request)
    }

    @RequestMapping(value = ["/detail/{id}"], method = [RequestMethod.GET])
    fun list(
            @PathVariable("id") productId: Long,
            model: Model,
            response: HttpServletResponse,
            request: HttpServletRequest
    ): String {
        val product = productDtoService?.findById(productId)

        var actualPrice = BigDecimal.ZERO
        // Discount price handling
        if (!BigDecimal("0.00").equals(product!!.discount)) {
            model.addAttribute("isDiscountProduct", true)
            actualPrice = productDtoService?.calculatePriceWithDiscount(product.price!!, product.discount!!)
        } else {
            actualPrice = product.price
        }

        // get current user

        val user = userService?.currentUser()

        // get product resources (checked sales record)
        try {
            val productResourcesList = productResourcesRequestService?.getProductResource(user!!.id!!, productId)
            model.addAttribute("productResources", productResourcesList)
        } catch (prade : ProductResourcesAccessDeniedException) {
            model.addAttribute("userPurchased" , false)
        }

        // Get product resource list [user sales trx list checking]
        // return null if user not purchase it yet
        model.addAttribute(PAGE_HEADER, "Product Detail")
        model.addAttribute(AdvancedBaseRestController.BREABCRUMB, breadCrumbService?.getProductDetailBreadCrumb())

        model.addAttribute("actualPrice", actualPrice)
        model.addAttribute("product", product)
        return getViewPath("product/detail", request)
    }

    @RequestMapping(value = ["/resources"], method = [RequestMethod.POST])
    fun getResources(@Valid productResourcesRequestDto: ProductResourcesRequestDto) {


    }

    @GetMapping(value = ["/{productId}/attachment/{uploadId}"])
    fun getAttachment(response: HttpServletResponse,
                      @PathVariable productId: Long,
                      @PathVariable uploadId: String,
                      attr : RedirectAttributes) : Any {

        val user = userService?.currentUser()

        if (!productResourcesRequestService?.checkUserOwnedProduct(user!!.id!!, productId)!!) {
            val errMsg = "User ${user!!.id} requested Product ID $productId resources - [ACCESS DENIED - User not yet purchased requested product]"
            log.error(errMsg)
            throw ProductResourcesAccessDeniedException(errMsg)
        }

        val fileMeta = productResourcesService?.getFileInfo(uploadId)
        val fileByte = productResourcesService?.downloadFile(uploadId)
        val resource = ByteArrayResource(fileByte!!)

        if (fileByte.isNotEmpty()) {

            // TODO:: in-line preview for PDF, MP3, JPG

            return ResponseEntity.ok()
                    .header(HttpHeaders.CONTENT_DISPOSITION,
                            "attachment;filename=\"" + StringUtils.trim(fileMeta!!.fileName!!) + "\"")
                    .contentType(MediaType.APPLICATION_OCTET_STREAM).contentLength(fileMeta.fileSize!!)
                    .body(resource)
        } else {

            attr.addAttribute(SYSTEM_MSG_KEY, SYSTEM_MSG_KEY).addFlashAttribute(SYSTEM_MSG_KEY, MessageBox(MessageBoxTypeEnum.WARNING, "Attachment not found"))
            // redirect to product resources form (through Controller)
            return "redirect:/message"
        }

    }

}