package com.samaxxw.app.controller

import com.samaxxw.app.config.SessionKey
import com.samaxxw.app.dto.CheckoutItemDto
import com.samaxxw.app.service.CheckoutService
import com.samaxxw.controller.AbstractRestHandler
import com.samaxxw.controller.AdvancedBaseRestController
import com.samaxxw.controller.AdvancedBaseRestController.Companion.SYSTEM_MSG_KEY
import com.samaxxw.dto.form.common.MessageBox
import com.samaxxw.dto.form.common.MessageBoxTypeEnum
import com.samaxxw.dtoservice.ProductDtoService
import com.samaxxw.exception.ValidationException
import com.samaxxw.service.PayPalService
import com.samaxxw.service.ProductService
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.core.context.SecurityContext
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.servlet.mvc.support.RedirectAttributes
import java.math.BigDecimal
import javax.servlet.http.HttpServletRequest

@Controller
@RequestMapping("/checkout")
class CheckoutController : ClientAbstractRestHandler() {

    @Autowired
    var checkoutService : CheckoutService? = null

    @Autowired
    var productDtoService : ProductDtoService? = null

    /**
     * Single Product checkout (temporary not using this)
     */
    @GetMapping("/product/{productId}")
    fun singleCheckoutPage(
            @PathVariable("productId") productId: Long,
            model: Model,
            request: HttpServletRequest
    ): String {

        try {

            // Clear checkout session

            // Create sales trx record

            // Check current user has already owned this product

            var checkoutItem = CheckoutItemDto()

            val productDto = productDtoService?.findById(productId)

            model.addAttribute("product", productDto)

            checkoutItem = checkoutService?.validateCheckoutItem(productId)!!

            // Discount price handling
            if (!BigDecimal("0.00").equals(checkoutItem.productDto!!.discount)) {
                model.addAttribute("discountPrice", checkoutItem.actualPrice)
            }

            // Save and replace to checkout session
            request.session.setAttribute(SessionKey.SESSION_CHECKOUT, checkoutItem)

        } catch (e: Exception) {
            e.printStackTrace()
            model.addAttribute("errorMsg", e.message)
        }

        model.addAttribute(AdvancedBaseRestController.PAGE_HEADER, "Checkout")
        model.addAttribute(AdvancedBaseRestController.BREABCRUMB, breadCrumbService?.getCheckoutRootBreadCrumb(productId))

        return getViewPath("purchase/checkout", request)
    }

    /**
     * Pass total value to PayPal
     */

    // TODO:: revamp to ajax control
    // Return redirect url for ajax handling.

//    @PostMapping()
//    fun checkout(request: HttpServletRequest, model : Model, attr : RedirectAttributes ): String {
//
//        val checkoutItem = request.session.getAttribute(SessionKey.SESSION_CHECKOUT) as CheckoutItemDto
//
//        try {
//            val result = checkoutService?.checkoutProcess()
//            return "redirect:" + result!!["redirect_url"].toString() // redirect url returned by PayPal API
//        } catch (e: Exception) {
////            model.addAttribute(AdvancedBaseRestController.SYSTEM_MSG_KEY, MessageBox(MessageBoxTypeEnum.DANGER, "[Checkout Failed] - ${e.message} "))
//            if (checkoutItem.productDto != null) {
//                attr.addAttribute(AdvancedBaseRestController.SYSTEM_MSG_KEY, AdvancedBaseRestController.SYSTEM_MSG_KEY).addFlashAttribute(AdvancedBaseRestController.SYSTEM_MSG_KEY, MessageBox(MessageBoxTypeEnum.DANGER, "Error - ${e.message}"))
//                return "redirect:/checkout/product/${checkoutItem.productDto!!.id}"
//            } else {
//                model.addAttribute(SYSTEM_MSG_KEY, e.message)
//                return "base/message"
//            }
//        }
//
//    }
    @PostMapping()
    fun checkout(request: HttpServletRequest, model : Model, attr : RedirectAttributes ): ResponseEntity<String> {
        val checkoutItem = request.session.getAttribute(SessionKey.SESSION_CHECKOUT) as CheckoutItemDto

        try {
            val result = checkoutService?.checkoutProcess()
            return ResponseEntity(result!!["redirect_url"].toString(), HttpStatus.OK) // redirect url returned by PayPal API
        } catch (e: Exception) {
            if (checkoutItem.productDto != null) {
                attr.addAttribute(AdvancedBaseRestController.SYSTEM_MSG_KEY, AdvancedBaseRestController.SYSTEM_MSG_KEY).addFlashAttribute(AdvancedBaseRestController.SYSTEM_MSG_KEY, MessageBox(MessageBoxTypeEnum.DANGER, "Error - ${e.message}"))
                return ResponseEntity("/checkout/product/${checkoutItem.productDto!!.id}", HttpStatus.BAD_REQUEST)
            } else {
                return ResponseEntity("Product not found.", HttpStatus.NOT_FOUND)
            }
        }

    }


    @GetMapping("/complete")
    fun complete(
            request: HttpServletRequest,
            model: Model
    ): String {

        try {
            checkoutService?.completeCheckout()
            model.addAttribute(AdvancedBaseRestController.SYSTEM_MSG_KEY, MessageBox(MessageBoxTypeEnum.SUCCESS, "Payment Success"))
            model.addAttribute("purchasedItemPage", "/purchase/user/list")
            return "paypal/paymentSuccess"

        } catch (e: Exception) {
            e.printStackTrace()
            model.addAttribute(AdvancedBaseRestController.SYSTEM_MSG_KEY, MessageBox(MessageBoxTypeEnum.DANGER, "[Payment Failed] - ${e.message} "))
        }

        return "base/message"
    }

    /**
     * Multiple Product checkout
     * Get list from session / db
     */

}
