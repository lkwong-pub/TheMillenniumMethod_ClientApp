package com.samaxxw.app

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.thymeleaf.ThymeleafProperties
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean
import org.thymeleaf.templateresolver.FileTemplateResolver
import org.thymeleaf.templateresolver.ITemplateResolver


@SpringBootApplication(scanBasePackages= ["com.samaxxw"])
class DemoResourceServerApplication

@Autowired
private val properties: ThymeleafProperties? = null

@Value("\${spring.thymeleaf.templates_root}")
private val templatesRoot: String? = null

fun main(args: Array<String>) {
    runApplication<DemoResourceServerApplication>(*args)
}

@Bean
fun defaultTemplateResolver(): ITemplateResolver {
    val resolver = FileTemplateResolver()
    resolver.suffix = properties!!.suffix
    resolver.prefix = templatesRoot
    resolver.setTemplateMode(properties.mode)
    resolver.isCacheable = properties.isCache
    return resolver
}