function create_menu(basepath, language)
{
	var base = (basepath == 'null') ? '' : basepath;

	document.write(
		'<table cellpadding="0" cellspaceing="0" border="0" style="width:98%"><tr>' +
		'<td class="td" valign="top">' +

		'<ul>' +
        '<li><a href="'+base+'logout">Logout</a></li>' +
		'<li><a href="'+base+'themethod/'+language+'/forward.html">Forward</a></li>' +	
		'<li><a href="'+base+'themethod/'+language+'/welcome.html">Welcome</a></li>' +
		'</ul>' +	

		'<h3>Section 1 - s1</h3>' +
		'<ul>' +
			'<li><a href="'+base+'themethod/'+language+'/l1wtbnap.html">Where the Bagpipe Notes are Positioned</a></li>' +
		'</ul>' +	
		
		'<h3>Section 2 - s2</h3>' +
		'<ul>' +
			'<li><a href="'+base+'themethod/'+language+'/l2hthtc.html">How to Hold the Chanter</a></li>' +
		'</ul>' +
		
		'<h3>Section 3 - s3</h3>' +
		'<ul>' +
			'<li><a href="'+base+'themethod/'+language+'/l3stnn.html">Sounding the Nine Notes</a></li>' +
			'<li><a href="'+base+'themethod/'+language+'/l3stnn-lgn.html">Low G Note</a></li>' +			
			'<li><a href="'+base+'themethod/'+language+'/l3stnn-lan.html">Low A Note</a></li>' +			
			'<li><a href="'+base+'themethod/'+language+'/l3stnn-bn.html">B Note</a></li>' +			
			'<li><a href="'+base+'themethod/'+language+'/l3stnn-en.html">E Note</a></li>' +			
			'<li><a href="'+base+'themethod/'+language+'/l3stnn-fn.html">F Note</a></li>' +			
			'<li><a href="'+base+'themethod/'+language+'/l3stnn-hgn.html">High G Note</a></li>' +			
			'<li><a href="'+base+'themethod/'+language+'/l3stnn-han.html">High A Note</a></li>' +
			'<li><a href="'+base+'themethod/'+language+'/l3stnn-cn.html">C Note</a></li>' +
			'<li><a href="'+base+'themethod/'+language+'/l3stnn-dn.html">D Note</a></li>' +
		'</ul>' +	
		
		'</td><td class="td_sep" valign="top">' +

		'<h3>Section 4 - s4</h3>' +
		'<ul>' +
			'<li><a href="'+base+'themethod/'+language+'/l4gn.html">Gracenotes</a></li>' +
			'<li><a href="'+base+'themethod/'+language+'/l4gn-hgg.html">High G Gracenote</a></li>' +
			'<li><a href="'+base+'themethod/'+language+'/l4gn-hag.html">High A Gracenote</a></li>' +
			'<li><a href="'+base+'themethod/'+language+'/l4gn-fg.html">F Gracenote</a></li>' +
		'</ul>' +		

		'<h3>Section 5 - s5</h3>' +
		'<ul>' +
			'<li><a href="'+base+'themethod/'+language+'/l5edg.html">E & D Gracenotes</a></li>' +
			'<li><a href="'+base+'themethod/'+language+'/l5edg-dg.html">D Gracenote</a></li>' +
			'<li><a href="'+base+'themethod/'+language+'/l5edg-eg.html">E Gracenote</a></li>' +
		'</ul>' +	

		'<h3>Section 6 - s6</h3>' +
		'<ul>' +
			'<li><a href="'+base+'themethod/'+language+'/l6te.html">Timing Exercises</a></li>' +		
		'</ul>' +	
		
		'<h3>Section 7 - s7</h3>' +
		'<ul>' +
			'<li><a href="'+base+'themethod/'+language+'/l7tb.html">The Birl</a></li>' +		
		'</ul>' +	
		
		'<h3>Section 8 - s8</h3>' +
		'<ul>' +
			'<li><a href="'+base+'themethod/'+language+'/l8tbbos.html">The Blue Bells of Scotland Part 1</a></li>' +		
		'</ul>' +	
		
		'<h3>Section 9 - s9</h3>' +
		'<ul>' +
			'<li><a href="'+base+'themethod/'+language+'/l9sn.html">Skip Notes</a></li>' +		
		'</ul>' +	
		
		'</td><td class="td_sep" valign="top">' +
		
		'<h3>Section 10 - s10</h3>' +
		'<ul>' +
			'<li><a href="'+base+'themethod/'+language+'/l10tbbos.html">The Blue Bells of Scotland Part 2</a></li>' +		
		'</ul>' +	
		
		'<h3>Section 11 - s11</h3>' +
		'<ul>' +
			'<li><a href="'+base+'themethod/'+language+'/l11fj.html">Frere Jacques</a></li>' +		
		'</ul>' +
		
		'<h3>Section 12 - s12</h3>' +
		'<ul>' +
			'<li><a href="'+base+'themethod/'+language+'/l12thaotg.html">The High A or Thumb Gracenote</a></li>' +		
		'</ul>' +	
		
		'<h3>Section 13 - s13</h3>' +
		'<ul>' +
			'<li><a href="'+base+'themethod/'+language+'/l13d.html">Doublings</a></li>' +		
		'</ul>' +	
		
		'<h3>Section 14 - s14</h3>' +
		'<ul>' +
			'<li><a href="'+base+'themethod/'+language+'/l14tdt.html">The D Throw</a></li>' +		
		'</ul>' +	
		
		'<h3>Section 15 - s15</h3>' +
		'<ul>' +
			'<li><a href="'+base+'themethod/'+language+'/l15cb.html">Cutting Bracken</a></li>' +		
		'</ul>' +	
		
		'<h3>Section 16 - s16</h3>' +
		'<ul>' +
			'<li><a href="'+base+'themethod/'+language+'/l16tlhsod.html">The Light and Heavy Strike on D</a></li>' +		
		'</ul>' +		
		
		'<h3>Section 17 - s17</h3>' +
		'<ul>' +
			'<li><a href="'+base+'themethod/'+language+'/l17ag.html">Amazing Grace</a></li>' +		
		'</ul>' +	
		
		'</td><td class="td_sep" valign="top">' +														
			
		'<h3>Section 18 - s18</h3>' +
		'<ul>' +
			'<li><a href="'+base+'themethod/'+language+'/l18tt.html">The Taorluath</a></li>' +		
		'</ul>' +			
		
		'<h3>Section 19 - s19</h3>' +
		'<ul>' +
			'<li><a href="'+base+'themethod/'+language+'/l19ctp.html">Compound Tune Preperation</a></li>' +		
		'</ul>' +			
			
		'<h3>Section 20 - s20</h3>' +
		'<ul>' +
			'<li><a href="'+base+'themethod/'+language+'/l20sbs.html">Skye Boat Song</a></li>' +		
		'</ul>' +				

		'<h3>Section 21 - s21</h3>' +
		'<ul>' +
			'<li><a href="'+base+'themethod/'+language+'/l21tm.html">The March of the 42nd Gordon Highlanders</a></li>' +		
		'</ul>' +

		'<h3>Section 22 - s22</h3>' +
		'<ul>' +
			'<li><a href="'+base+'themethod/'+language+'/l22g.html">Grips</a></li>' +		
		'</ul>' +		
		
		'<h3>Section 23 - s23</h3>' +
		'<ul>' +
			'<li><a href="'+base+'themethod/'+language+'/l23wtbio.html">When the Battle is O\'er</a></li>' +		
		'</ul>' +		

		'<h3>Section 24 - s24</h3>' +
		'<ul>' +
			'<li><a href="'+base+'themethod/'+language+'/l24plb.html">Paddy\'s Leather Breeches</a></li>' +		
		'</ul>' +	
		
		'<h3>Section 25 - s25</h3>' +
		'<ul>' +
			'<li><a href="'+base+'themethod/'+language+'/l25p.html">100 Piper\'s</a></li>' +		
		'</ul>' +		

		'<h3>Section 26 - s26</h3>' +
		'<ul>' +
			'<li><a href="'+base+'themethod/'+language+'/l26als.html">Auld Lang Synne</a></li>' +		
		'</ul>' +	


		
		'</td></tr></table>');
}