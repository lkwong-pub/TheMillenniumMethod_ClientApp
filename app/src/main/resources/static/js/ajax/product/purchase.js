/**
 * Prompt dialog before redirect to PayPal
 */
$(document).on('click', '#purchase-checkout', function (event) {

    event.preventDefault();

    swal({
        title: "Confirm purchase?",
        text: "You will be redirected to PayPal website!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Checkout!",
        closeOnConfirm: false

    }, function () {

        dimBackground();
        // e.preventDefault(); // avoid to execute the actual submit of the form.

        var form = $('#purchase-checkout-form');
        var url = form.attr('action');
        var method = form.attr('method');

        $.ajax({
            type: method,
            url: url,
            data: form.serialize(), // serializes the form's elements.
            success: function (data) {
                // console.log(data); // show response from server
                window.location.href = data;
            },
            error: function (jqXHR, textStatus, errorThrown) {
                unDimBackground();
                swal({
                    title: "Error",
                    text: jqXHR.responseText,
                    type: "error"
                });
            }

        });

    });

});