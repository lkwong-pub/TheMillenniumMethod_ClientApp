$(document).on('click', '#resendVerification', function (event) {

    event.preventDefault();

    var endpoint = "/user/verification/resend";

    console.log(endpoint);
    // $.post('/page-resources/delete/batch', test);

    executeAjaxDialog(
        "GET",
        endpoint,
        "Resend user verification e-mail.",
        "Confirm?",
        null,
        "OK",
        "Yes",
        "Verification e-mail sent. Please check your email address.",
        null)

});