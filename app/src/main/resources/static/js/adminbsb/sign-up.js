$(function () {


    // grecaptcha.ready(function() {
    //     console.log("grecaptcha ready - key " + $('#sitekey').val());
    //     grecaptcha.execute($('#sitekey').val() , {action:'validate_captcha'})
    //         .then(function(token) {
    //             // add token value to form
    //             $('#token').val(token)
    //             // document.getElementById('g-recaptcha-response').value = token;
    //         });
    //     console.log("token - " + $('#token').val());
    // });
    // grecaptcha.ready(function() {
    //     $('#token').val($('#recaptcha-token').val());
    // });

    $('#sign_up').validate({
        rules: {
            'terms': {
                required: true
            },
            'confirm': {
                equalTo: '[name="password"]'
            }
        },
        highlight: function (input) {
            console.log(input);
            $(input).parents('.form-line').addClass('error');
        },
        unhighlight: function (input) {
            $(input).parents('.form-line').removeClass('error');
        },
        errorPlacement: function (error, element) {
            $(element).parents('.input-group').append(error);
            $(element).parents('.form-group').append(error);
        }
    });
});