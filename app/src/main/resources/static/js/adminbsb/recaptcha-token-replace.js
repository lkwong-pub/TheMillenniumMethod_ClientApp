$(function () {


    grecaptcha.ready(function () {
        console.log("grecaptcha ready - key " + $('#sitekey').val());
        grecaptcha.execute($('#sitekey').val(), {action: 'validate_captcha'})
            .then(function (token) {
                // add token value to form
                $('#token').val(token)
                // document.getElementById('g-recaptcha-response').value = token;
            });
        console.log("token - " + $('#token').val());
    });

});