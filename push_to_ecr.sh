./gradlew clean bootWar -x test
docker build -t millennium-method/client .
docker tag millennium-method/client:latest 278131614032.dkr.ecr.us-east-2.amazonaws.com/the-millennium-resources-server-client-uat:latest
$(aws ecr get-login --no-include-email)
docker push 278131614032.dkr.ecr.us-east-2.amazonaws.com/the-millennium-resources-server-client-uat:latest
