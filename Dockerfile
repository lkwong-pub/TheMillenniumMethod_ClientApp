FROM java:8
EXPOSE 8080

ADD app/build/libs/app-0.1.jar mm-client.jar

ENTRYPOINT ["java", "-jar", "mm-client.jar"]