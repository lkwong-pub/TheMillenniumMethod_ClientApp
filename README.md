# The Millennium Method Application

An application for customer to purchase products and access the tutorial resources.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. 

See deployment for notes on how to deploy the project on a live system.

Clone repository from GitLab
`git clone git@gitlab.com:millenniummethod/TheMillenniumMethod_ClientApp.git`



### Prerequisites

IDE
```
IntelliJ

```
DB 
```
(Docker container preferred)
MySQL
MongoDB
```
Other
```
(Docker container preferred)
Redis
```

### Installing

A step by step series of examples that tell you how to get a development env running

1. Open build.gradle with IntelliJ or another IDE

2. Import all gradle dependencies

3. Create schema on MySQL. (Schema name refer to application-dev.properties)

4. Run
`./gradlew bootRun`

Open your browser and go to 
`http://localhost:8100`


## Deployment

UAT and Live are deployed on Kubernetes hosted by DigitalOcean.

Run 
`./push_to_ecr_git_tag.sh`

Script will build and package as a docker image and push to AWS ECR
Git commit short SHA will be provided and please replace to {{GIT_COMMIT_SHORT_SHA}} deployment command below 

Update DigitalOcean K8S credential

```
doctl kubernetes cluster kubeconfig show 00e8bb73-051d-43cf-ad52-4e48408d7656 > k8s-1-13-3-do-0-ams3-1551792493106-kubeconfig.yaml -t b358882c166a519de4d763becc94ea8e782813a31e741a25035ef8492ab00e15
```

Deploy to K8S
```

kubectl --kubeconfig="k8s-1-13-3-do-0-ams3-1551792493106-kubeconfig.yaml" set image \
deployment mm-client \
mm-client=278131614032.dkr.ecr.us-east-2.amazonaws.com/the-millennium-resources-server-client-uat:{{GIT_COMMIT_SHORT_SHA}} --record

```

Track updated image roll out status
```
kubectl --kubeconfig="k8s-1-13-3-do-0-ams3-1551792493106-kubeconfig.yaml" rollout status deployment mm-client
```

Add additional notes about how to deploy this on a live system

## Built With

* [Spring](https://spring.io/) - The framework used
* [Gradle](https://gradle.org/) - Dependency Management
* [AdminBSB](https://github.com/gurayyarar/AdminBSBMaterialDesign) - Used for front end

## Contributing

Not capable 

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags). 

## Authors

* **Lai Kit Wong** - *Initial work*

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Miscellaneous
##### Hot swap
    use command './gradlew -t classes' for listen the changes
    run gradlew bootRun in debug mode
    
##### PayPal sandbox buyer account
    E-mail : millennium-buyer-test@millennium.com
    Password : millennium
    
##### Expose service from K8S
    kubectl expose deployment mm-client --type=LoadBalancer --name=mm-client-service
    kubectl expose deployment mm-client --port=8100 --target-port=8080 \
        --name=mm-client-service --type=LoadBalancer