docker stop millennium-method-client
docker rm millennium-method-client
docker run \
-p 8100:8080 \
-e CATALINA_OPTS="\
-Dspring.profiles.active=dev-docker \
" \
--name millennium-method-client \
--link mysql:mysql \
--link minio:minio \
--link resources-server:resources-server \
millennium-method/client:latest