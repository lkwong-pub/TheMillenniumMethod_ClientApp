GIT_VERSION=$(git rev-parse --verify --short HEAD)
docker tag millennium-method/client:latest 278131614032.dkr.ecr.us-east-2.amazonaws.com/the-millennium-resources-server-client-uat:$GIT_VERSION
$(aws ecr get-login --no-include-email)
docker push 278131614032.dkr.ecr.us-east-2.amazonaws.com/the-millennium-resources-server-client-uat:$GIT_VERSION
echo $GIT_VERSION

